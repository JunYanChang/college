@extends('layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=grade_li]').addClass('active');
            $('div[name=grade]').addClass('show');
        });
    </script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">成績管理</li>
        <li class="breadcrumb-item text_label">德行成績</li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <form class="form-inline" action="{{ route('grade_work') }}" id="term">
                        <div class="input-group">
                            <label for="sel1">班級:</label>
                            <select class="form-control" id="sel1">
                                <option>基礎班一</option>
                                <option>基礎班二</option>
                                <option>本科班一</option>
                                <option>本科班二</option>
                                <option>本科班三</option>
                                <option>專科班一</option>
                                <option>專科班二</option>
                                <option>專科班三</option>
                                <option>專科班四</option>
                            </select>
                        </div>
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="查詢學號.....">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit"><i class="fas fa-search"></i></button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-4">
                </div>
                <div class="col-12" style="padding-top: 1%;text-align: center">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>班級</th>
                            <th>學號</th>
                            <th>姓名</th>
                            <th>德行平均成績</th>
                            <th>詳細成績表</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>基礎班一</td>
                                <td>eb123456</td>
                                <td>Doe</td>
                                <td>A</td>
                                <td><a href="{{ route('de_sing_list_grade') }}" class="btn btn-lg" style="color: #1d68a7"><i class="fas fa-clipboard-list"></i></a></td>
                            </tr>
                            <tr>
                                <td>基礎班一</td>
                                <td>eb123456</td>
                                <td>Moe</td>
                                <td>A</td>
                                <td><a href="{{ route('de_sing_list_grade') }}" class="btn btn-lg" style="color: #1d68a7"><i class="fas fa-clipboard-list"></i></a></td>
                            </tr>
                            <tr>
                                <td>基礎班一</td>
                                <td>rb123456</td>
                                <td>Dooley</td>
                                <td>A</td>
                                <td><a href="{{ route('de_sing_list_grade') }}" class="btn btn-lg" style="color: #1d68a7"><i class="fas fa-clipboard-list"></i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

@extends('layouts.layout')
@push('script')
<script>
    $(document).ready(function() {
        $('li[name=student_li]').addClass('active');
        $('div[name=student]').addClass('show');
    });
</script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">學生管理</li>
        <li class="breadcrumb-item">修復學管理</li>
        <li class="breadcrumb-item text_label">復學</li>
    </ol>
@endsection
@section('content')
    <div class="row align-items-stretch">
        <div class="col-12">
            <div class="row">
                <div class="col-4">
                </div>
                <div class="col-8">
                    <form class="form-inline" action="" style="float: right">
                        <label for="search" class="mr-sm-2">搜尋：</label>
                        <input type="text" class="form-control mb-2 mr-sm-2" id="search" placeholder="姓名\學號\電子郵件\電話" size="30">
                        <button type="submit" class="btn btn-primary mb-2"><i class="fa fa-search"></i></button>
                    </form>
                </div>

                <div class="col-12">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>管理</th>
                            <th>審核</th>
                            <th>核准</th>
                            <th>學號</th>
                            <th>年限</th>
                            <th>休學原因</th>
                            <th>申請日期</th>
                            <th>休學日期</th>
                            <th>通過日期</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>
                                <a href="#" style="color: red;font-size: 1.3rem"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;
                            </td>
                            <td>
                                <a href="#" style="color: green;font-size: 1.3rem"><i class="far fa-calendar-check"></i></a>&nbsp;&nbsp;&nbsp;
                                <a href="#" style="color: blue;font-size: 1.3rem"><i class="far fa-calendar-times"></i></a>
                            </td>
                            <td>待核定</td>
                            <td>eb42282</td>
                            <td>1</td>
                            <td>測試</td>
                            <td>2018/9/26</td>
                            <td>2018/9/26</td>
                            <td></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

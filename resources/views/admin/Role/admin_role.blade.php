@extends('admin.layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=management_li]').addClass('active');
            $('div[name=management]').addClass('show');

        });
    </script>

@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">網路系統管理</li>
        <li class="breadcrumb-item text_label"><b>角色權限</b></li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-sm-4">
                    <h3>角色權限</h3>
                </div>
                <div class="col-12 col-sm-8 text-right">
                    <a href="{{ route('role.create') }}" class="btn btn-primary"><i class="fas fa-plus"></i>新增角色與權限</a>
                </div>
                <div class="col-12" style="padding-top: 1rem">

                    <div class="card-columns">
                        @foreach($data as $v)
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">{{ $v->class }}</h4>
                                    <ul class="card-text">
                                        <li>權限</li>
                                    </ul>
                                    <a href="" class="btn btn-warning">編輯</a>
                                    {{--{{ route('admin_role_edit') }}--}}
                                    <a href="#" class="btn btn-danger">刪除</a>
                                </div>
                            </div>
                        @endforeach


                    </div>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

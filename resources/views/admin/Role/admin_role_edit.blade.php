@extends('layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=management_li]').addClass('active');
            $('div[name=management]').addClass('show');

            $("#clickAllfunction").click(function() {
                if($("#clickAllfunction").prop("checked")) {
                    $("input[name='function[]']").prop("checked", true);
                }else{
                    $("input[name='function[]']").prop("checked", false);
                }
            });

            $("#clickAllclass").click(function() {
                if($("#clickAllclass").prop("checked")) {
                    $("input[name='class[]']").prop("checked", true);
                }else{
                    $("input[name='class[]']").prop("checked", false);
                }
            });
        });
    </script>

@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">網路系統管理</li>
        <li class="breadcrumb-item">角色權限</li>
        <li class="breadcrumb-item text_label"><b>編輯角色與權限</b></li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <form class="row">
                <div class="col-8">
                    <a href="{{ route('admin_role') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i>返回</a>
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-primary" style="float: right"><i class="fas fa-plus"></i>新增</button>
                </div>
                <div class="col-12" style="padding-top: 1rem">

                    <div class="form-group">
                        <label for="class">階級：</label>
                        <select class="form-control" id="class">
                            <option>系統管理員</option>
                            <option>行政人員</option>
                            <option>助教人員</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="Character">角色名稱：</label>
                        <input type="text" class="form-control" id="Character">
                    </div>
                    <h3>功能</h3>
                    <div class=" custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="clickAllfunction" name="clickAllfunction">
                        <label class="custom-control-label" for="clickAllfunction">全部</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="function1" name="function[]">
                        <label class="custom-control-label" for="function1">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="function2" name="function[]">
                        <label class="custom-control-label" for="function2">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="function3" name="function[]">
                        <label class="custom-control-label" for="function3">Check this custom checkbox</label>
                    </div>
                    <h3>課程</h3>
                    <div class=" custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="clickAllclass" name="clickAllclass">
                        <label class="custom-control-label" for="clickAllclass">全部</label>
                    </div>
                    <div class=" custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="class1" name="class[]">
                        <label class="custom-control-label" for="class1">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="class2" name="class[]">
                        <label class="custom-control-label" for="class2">Check this custom checkbox</label>
                    </div>
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input" id="class3" name="class[]">
                        <label class="custom-control-label" for="class3">Check this custom checkbox</label>
                    </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

@extends('admin.layouts.layout')

@push('script')
    <script src="/js/select.js"></script>
    <script>
        $(document).ready(function () {
            $('li[name=management_li]').addClass('active');
            $('div[name=management]').addClass('show');
        });
    </script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">網路系統管理</li>
        <li class="breadcrumb-item">角色權限</li>
        <li class="breadcrumb-item text_label"><b>新增角色與權限</b></li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <form class="row" action="{{ route('role.store') }}" method="post">
                @csrf
                <div class="col-8">
                    <a href="{{ route('role.index') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i>返回</a>
                </div>
                <div class="col-4">
                    <button type="submit" class="btn btn-primary" style="float: right"><i class="fas fa-plus"></i>新增</button>
                </div>
                <div class="col-12 pt-3">
                    <div class="form-group">
                        <label for="Character">角色名稱：</label>
                        <input type="text" class="form-control" id="Character">
                    </div>
                    <h3 class="pb-2">功能</h3>
                    <div class="row pb-4">
                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">學生管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-4">
                                    <div class="row mb-0">
                                        <div class="col-6"><h5>學生資料</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="student_data_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class="custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="student_data1" name="student_data[]" value="review">
                                        <label class="custom-control-label" for="student_data1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="student_data2" name="student_data[]" value="edit">
                                        <label class="custom-control-label" for="student_data2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="student_data3" name="student_data[]" value="delete">
                                        <label class="custom-control-label" for="student_data3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>黑名單</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="black_list_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="black_list1" name="black_list[]" value="review">
                                        <label class="custom-control-label" for="black_list1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="black_list2" name="black_list[]" value="edit">
                                        <label class="custom-control-label" for="black_list2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="sblack_list3" name="black_list[]" value="delete">
                                        <label class="custom-control-label" for="black_list3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>休學管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="drop_out_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_out1" name="drop_out[]" value="review">
                                        <label class="custom-control-label" for="drop_out1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_out2" name="drop_out[]" value="edit">
                                        <label class="custom-control-label" for="drop_out2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_out3" name="drop_out[]" value="delete">
                                        <label class="custom-control-label" for="drop_out3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>復學管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="drop_in_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_in1" name="drop_in[]" value="review">
                                        <label class="custom-control-label" for="drop_in1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_in2" name="drop_in[]" value="edit">
                                        <label class="custom-control-label" for="drop_in2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_in3" name="drop_in[]" value="delete">
                                        <label class="custom-control-label" for="drop_in3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>歷史紀錄</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="recode_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode1" name="recode[]" value="review">
                                        <label class="custom-control-label" for="recode1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode2" name="recode[]" value="edit">
                                        <label class="custom-control-label" for="recode2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode3" name="recode[]" value="delete">
                                        <label class="custom-control-label" for="recode3">刪除</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">科目學分課程</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>學科類別</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="course_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course1" name="course[]" value="review">
                                        <label class="custom-control-label" for="course1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course2" name="course[]" value="edit">
                                        <label class="custom-control-label" for="course2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course3" name="course[]" value="delete">
                                        <label class="custom-control-label" for="course3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>學科年級</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="course_level_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_level1" name="course_level[]" value="review">
                                        <label class="custom-control-label" for="course_level1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_level2" name="course_level[]" value="edit">
                                        <label class="custom-control-label" for="course_level2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_level3" name="course_level[]" value="delete">
                                        <label class="custom-control-label" for="course_level3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-7"><h5>課程影音資料庫</h5></div>
                                        <div class="col-5 text-right"><button type="button" class="btn btn-primary" id="course_data_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_data1" name="course_data[]" value="review">
                                        <label class="custom-control-label" for="course_data1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_data2" name="course_data[]" value="edit">
                                        <label class="custom-control-label" for="course_data2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="course_data3" name="course_data[]" value="delete">
                                        <label class="custom-control-label" for="course_data3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>科目學分編輯</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="subject_class_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="subject_class1" name="subject_class[]" value="review">
                                        <label class="custom-control-label" for="subject_class1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="subject_class2" name="subject_class[]" value="edit">
                                        <label class="custom-control-label" for="subject_class2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="subject_class3" name="subject_class[]" value="delete">
                                        <label class="custom-control-label" for="subject_class3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-9"><h5>選課管理及輔助選課</h5></div>
                                        <div class="col-3 text-right"><button type="button" class="btn btn-primary btn-sm" id="sup_elective_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="sup_elective1" name="sup_elective[]" value="config">
                                        <label class="custom-control-label" for="sup_elective1">選課管理</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="sup_elective2" name="sup_elective[]" value="review">
                                        <label class="custom-control-label" for="sup_elective2">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="sup_elective3" name="sup_elective[]" value="edit">
                                        <label class="custom-control-label" for="sup_elective3">編輯</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">成績管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>課程作業成績</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="work_grade_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="work_grade1" name="work_grade[]" value="review">
                                        <label class="custom-control-label" for="work_grade1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="work_grade2" name="work_grade[]" value="edit">
                                        <label class="custom-control-label" for="work_grade2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="work_grade3" name="work_grade[]" value="delete">
                                        <label class="custom-control-label" for="work_grade3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>學生個人成績</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="students_grade_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_grade1" name="students_grade[]" value="review">
                                        <label class="custom-control-label" for="students_grade1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_grade2" name="students_grade[]" value="edit">
                                        <label class="custom-control-label" for="students_grade2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_grade3" name="students_grade[]" value="delete">
                                        <label class="custom-control-label" for="students_grade3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>德行成績</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="de_sing_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="de_sing1" name="de_sing[]" value="review">
                                        <label class="custom-control-label" for="de_sing1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="de_sing2" name="de_sing[]" value="edit">
                                        <label class="custom-control-label" for="de_sing2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="de_sing3" name="de_sing[]" value="delete">
                                        <label class="custom-control-label" for="de_sing3">刪除</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">升降級管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>學生升級管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="students_update_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_update1" name="students_update[]" value="review">
                                        <label class="custom-control-label" for="students_update1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_update2" name="students_update[]" value="edit">
                                        <label class="custom-control-label" for="students_update2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="students_update3" name="students_update[]" value="delete">
                                        <label class="custom-control-label" for="students_update3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>升級紀錄名單</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="recode_update_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode_update1" name="recode_update[]" value="review">
                                        <label class="custom-control-label" for="recode_update1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode_update2" name="recode_update[]" value="edit">
                                        <label class="custom-control-label" for="recode_update2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="recode_update3" name="recode_update[]" value="delete">
                                        <label class="custom-control-label" for="recode_update3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>升級贈品管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="gift_update_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="gift_update1" name="gift_update[]" value="review">
                                        <label class="custom-control-label" for="gift_update1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="gift_update2" name="gift_update[]" value="edit">
                                        <label class="custom-control-label" for="gift_update2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="gift_update3" name="gift_update[]" value="delete">
                                        <label class="custom-control-label" for="gift_update3">刪除</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">公告管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>公告管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="bulletin_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="bulletin1" name="bulletin[]" value="review">
                                        <label class="custom-control-label" for="bulletin1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="bulletin2" name="bulletin[]" value="edit">
                                        <label class="custom-control-label" for="bulletin2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="bulletin3" name="bulletin[]" value="delete">
                                        <label class="custom-control-label" for="bulletin3">刪除</label>
                                    </div>
                                </div>
                            </div>

                            <h3 class="pl-3 pt-4">信件管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>休復學信件</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="drop_mail_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_mail1" name="drop_mail[]" value="review">
                                        <label class="custom-control-label" for="drop_mail1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_mail2" name="drop_mail[]" value="edit">
                                        <label class="custom-control-label" for="drop_mail2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="drop_mail3" name="drop_mail[]" value="delete">
                                        <label class="custom-control-label" for="drop_mail3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>升級信件</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="update_mail_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="update_mail1" name="update_mail[]" value="review">
                                        <label class="custom-control-label" for="update_mail1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="update_mail2" name="update_mail[]" value="edit">
                                        <label class="custom-control-label" for="update_mail2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="update_mail3" name="update_mail[]" value="delete">
                                        <label class="custom-control-label" for="update_mail3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>新生入學信件</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="enrollment_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="enrollment1" name="enrollment[]" value="review">
                                        <label class="custom-control-label" for="enrollment1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="enrollment2" name="enrollment[]" value="edit">
                                        <label class="custom-control-label" for="enrollment2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="enrollment3" name="enrollment[]" value="delete">
                                        <label class="custom-control-label" for="enrollment3">刪除</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">頁面編輯</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>入學指導</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="guidance_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="guidance1" name="guidance[]" value="review">
                                        <label class="custom-control-label" for="guidance1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="guidance2" name="guidance[]" value="edit">
                                        <label class="custom-control-label" for="guidance2">編輯</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>教學計劃</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="plan_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="plan1" name="plan[]" value="review">
                                        <label class="custom-control-label" for="plan1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="plan2" name="plan[]" value="edit">
                                        <label class="custom-control-label" for="plan2">編輯</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>認識學院</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="understanding_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="understanding1" name="understanding[]" value="review">
                                        <label class="custom-control-label" for="understanding1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="understanding2" name="understanding[]" value="edit">
                                        <label class="custom-control-label" for="understanding2">編輯</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>年度行事曆</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="calendar_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="calendar1" name="calendar[]" value="review">
                                        <label class="custom-control-label" for="calendar1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="calendar2" name="calendar[]" value="edit">
                                        <label class="custom-control-label" for="calendar2">編輯</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>重要通知</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="announcement_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="announcement1" name="announcement[]" value="review">
                                        <label class="custom-control-label" for="announcement1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="announcement2" name="announcement[]" value="edit">
                                        <label class="custom-control-label" for="announcement2">編輯</label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">資源檔案管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>經本</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="scripture_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="scripture1" name="scripture[]" value="review">
                                        <label class="custom-control-label" for="scripture1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="scripture2" name="scripture[]" value="edit">
                                        <label class="custom-control-label" for="scripture2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="scripture3" name="scripture[]" value="delete">
                                        <label class="custom-control-label" for="scripture3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>教材</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="teaching_material_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="teaching_material1" name="teaching_material[]" value="review">
                                        <label class="custom-control-label" for="teaching_material1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="teaching_material2" name="teaching_material[]" value="edit">
                                        <label class="custom-control-label" for="teaching_material2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="teaching_material3" name="teaching_material[]" value="delete">
                                        <label class="custom-control-label" for="teaching_material3">刪除</label>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="col-12 col-12 col-sm-12 col-md-3 pl-3 pb-3">
                            <h3 class="pl-3">系統管理</h3>
                            <div class="row">
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>角色權限</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="role_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="role1" name="role[]" value="review">
                                        <label class="custom-control-label" for="role1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="role2" name="role[]" value="edit">
                                        <label class="custom-control-label" for="role2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="role3" name="role[]" value="delete">
                                        <label class="custom-control-label" for="role3">刪除</label>
                                    </div>
                                </div>
                                <div class="col-12 pl-5 pb-3">
                                    <div class="row">
                                        <div class="col-6"><h5>帳號管理</h5></div>
                                        <div class="col-6 text-right"><button type="button" class="btn btn-primary" id="account_selectAll">全選</button></div>
                                    </div>
                                    <hr>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="account1" name="account[]" value="review">
                                        <label class="custom-control-label" for="account1">預覽</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="account2" name="account[]" value="edit">
                                        <label class="custom-control-label" for="account2">編輯</label>
                                    </div>
                                    <div class=" custom-control custom-checkbox custom-control-inline">
                                        <input type="checkbox" class="custom-control-input" id="account3" name="account[]" value="delete">
                                        <label class="custom-control-label" for="account3">刪除</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 pb-3">
                            <h3>課程</h3>
                        </div>
                        @foreach($class as $c)
                            <div class="col-12 col-sm-12 col-md-4 pl-5 pb-3">
                                <div class="row">
                                    <div class="col-6"><h4>{{ $c->level }}</h4></div>
                                    <div class="col-6 text-right"><button type="button" class="btn btn-primary btn-sm" id="class_level_{{ $c->id }}_selectAll">全選</button></div>
                                </div>
                                <hr>
                                @foreach($c->curricula as $curricula)
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="class_course{{ $curricula->id }}" name="class_course_{{ $c->id }}[]">
                                        <label class="custom-control-label" for="class_course{{ $curricula->id }}">{{ $curricula->coursedata->title }}</label>
                                    </div>
                                @endforeach
                            </div>
                        @endforeach
                    </div>
                </div>
            </form>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

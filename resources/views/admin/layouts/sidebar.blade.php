<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">網路學院</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <li class="nav-item" name="dashboard_li">
        <a class="nav-link" href="{{ route('dashboard.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>統計數據</span>
        </a>
    </li>


    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="student_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#student" aria-expanded="true" aria-controls="student">
            <i class="fas fa-address-card"></i>
            <span>學生管理</span>
        </a>
        <div id="student" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar" name="student">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('data.index') }}">學生資料</a>
                <a class="collapse-item" href="{{ route('black_list.index') }}">黑名單</a>
                <h4 class="collapse-header">休復學管理</h4>
                <a class="collapse-item" href="{{ route('students_drop_out') }}">休學</a>
                <a class="collapse-item" href="{{ route('students_drop_in') }}">復學</a>
                <a class="collapse-item" href="{{ route('students_drop_all') }}">歷史紀錄</a>
{{--                <a class="collapse-item" href="{{ route('students_drop_mail_config') }}">信件通知</a>--}}
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="course_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#course" aria-expanded="true" aria-controls="course">
            <i class="fas fa-clipboard-list"></i>
            <span>課目學分課程</span>
        </a>
        <div id="course" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="course">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('course.index') }}">學科類別</a>
                <a class="collapse-item" href="{{ route('course_level.index') }}">學科年級</a>
                <a class="collapse-item" href="{{ route('course_data.index') }}">課程影音資料庫</a>
                <a class="collapse-item" href="{{ route('subject_class.index') }}">科目學分編輯</a>
                <a class="collapse-item" href="{{ route('sup_elective.index') }}">選課管理及輔助選課</a>
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="grade_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#grade" aria-expanded="true" aria-controls="grade">
            <i class="fas fa-file-alt"></i>
            <span>成績管理</span>
        </a>
        <div id="grade" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="grade">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('work_grade.index') }}">課程作業成績</a>
                <a class="collapse-item" href="{{ route('students_grade.index') }}">學生個人成績</a>
                <a class="collapse-item" href="{{ route('de_sing') }}">德行成績</a>
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="lift_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#lift" aria-expanded="true" aria-controls="lift">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>升降級管理</span>
        </a>
        <div id="lift" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="lift">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('students_update.index') }}">學生升級管理</a>
                <a class="collapse-item" href="{{ route('recode_update.index') }}">升級紀錄名單</a>

                <a class="collapse-item" href="">升級贈品管理</a>
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    {{--<li class="nav-item" name="discussion_li">--}}
        {{--<a class="nav-link" href="#" data-toggle="collapse" data-target="#discussion" aria-expanded="true" aria-controls="discussion">--}}
            {{--<i class="fas fa-fw fa-tachometer-alt"></i>--}}
            {{--<span>討論版管理</span>--}}
        {{--</a>--}}
        {{--<div id="discussion" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="discussion">--}}
            {{--<div class="bg-white py-2 collapse-inner rounded">--}}
                {{--<a class="collapse-item" href="{{ route('forum_black_list') }}">黑名單</a>--}}
                {{--<a class="collapse-item" href="{{ route('forum_description') }}">說明看板</a>--}}
                {{--<a class="collapse-item" href="{{ route('forum_theme') }}">主題編輯</a>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</li>--}}

    <li class="nav-item" name="announcement_li">
        <a class="nav-link" href="{{ route('bulletin.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>公告管理</span>
        </a>
    </li>

    <li class="nav-item" name="email_config_li">
        <a class="nav-link" data-toggle="collapse" data-target="#email_config" aria-expanded="true" aria-controls="email_config">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>信件管理</span>
        </a>
        <div id="email_config" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="email_config">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('email_config_drop') }}">休復學信件</a>
                <a class="collapse-item" href="{{ route('email_config_update') }}">升級信件</a>
                <a class="collapse-item" href="{{ route('email_config_enrollment') }}">新生入學信件</a>
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="edit_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#edit" aria-expanded="true" aria-controls="edit">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>頁面編輯</span>
        </a>
        <div id="edit" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="edit">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('edit_admission_guidance') }}">入學指導</a>
                <a class="collapse-item" href="{{ route('edit_plan') }}">教學計劃</a>
                <a class="collapse-item" href="{{ route('edit_understanding') }}">認識學院</a>
                <a class="collapse-item" href="{{ route('edit_calendar') }}">年度行事曆</a>
                <a class="collapse-item" href="{{ route('edit_description') }}">說明看板</a>
                <a class="collapse-item" href="{{ route('edit_announcement') }}">重要通知</a>
            </div>
        </div>
    </li>

    <!-- Dashboards Accordion Menu -->
    <li class="nav-item" name="download_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#download" aria-expanded="true" aria-controls="download">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>資源檔案管理</span>
        </a>
        <div id="download" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="download">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('scripture.index') }}">經本</a>
                <a class="collapse-item" href="{{ route('teaching_material.index') }}">教材</a>
            </div>
        </div>
    </li>

    <li class="nav-item" id="management_li">
        <a class="nav-link" href="#" data-toggle="collapse" data-target="#management" aria-expanded="true" aria-controls="management">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>網路系統管理</span>
        </a>
        <div id="management" class="collapse " aria-labelledby="headingOne" data-parent="#accordionSidebar" name="management">
            <div class="bg-white py-2 collapse-inner rounded">
                <a class="collapse-item" href="{{ route('role.index') }}">角色權限</a>
                <a class="collapse-item" href="{{ route('account.index') }}">帳號管理</a>
            </div>
        </div>
    </li>
</ul>

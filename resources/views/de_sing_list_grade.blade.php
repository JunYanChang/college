@extends('layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=grade_li]').addClass('active');
            $('div[name=grade]').addClass('show');

        });
    </script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">成績管理</li>
        <li class="breadcrumb-item">德行成績</li>
        <li class="breadcrumb-item text_label">個人德行詳細成績</li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <a href="{{ route('de_sing') }}" class="btn btn-primary"><i class="fas fa-arrow-left"></i>返回</a>
                </div>
                <div class="col-4">
                </div>
                <div class="col-12" style="padding-top: 1%;text-align: center">
                    <div class="d-flex" style="padding-bottom: 1%">
                        <div class="p-2 bg-warning flex-fill">班級：</div>
                        <div class="p-2 bg-warning flex-fill">學號：</div>
                        <div class="p-2 bg-warning flex-fill">姓名：</div>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>項目</th>
                            <th>最新一天成績</th>
                            <th>一個月平均成績</th>
                            <th>總平均成績</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>弟子規</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>感應篇</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>十善業</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>功過格</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                        </tr>
                        <tr>
                            <td>自我評量</td>
                            <td>100</td>
                            <td>100</td>
                            <td>100</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

@extends('layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=edit_li]').addClass('active');
            $('div[name=edit]').addClass('show');

            $('textarea').summernote({
                tabsize: 3,
                height: 600
            });
        });
    </script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">頁面編輯</li>
        <li class="breadcrumb-item text_label"><b>休學通知</b></li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <h2>課程介紹</h2>
                </div>
                <div class="col-4">
                    <a href="#" class="btn btn-primary" style="float: right"><i class="fas fa-save"></i> 保存</a>
                </div>
                <div class="col-12" style="padding-top: 1rem">
                    <ul class="nav nav-pills nav-justified">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home">基礎學科</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu1">本科學科</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#menu2">專科學科</a>
                        </li>
                    </ul>

                    <div class="tab-content" style="padding-top: 1%">
                        <div class="tab-pane container active" id="home">
                            <textarea id="message_edit" name="basis"></textarea>
                        </div>
                        <div class="tab-pane container fade" id="menu1">
                            <textarea id="message_edit" name="bachelor"></textarea>
                        </div>
                        <div class="tab-pane container fade" id="menu2">
                            <textarea id="message_edit" name="specialist"></textarea>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection

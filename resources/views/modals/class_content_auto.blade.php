<!-- The Modal -->
<div class="modal" id="auto_elective_course">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">快速排課</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form action="" method="post">

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="class">年級:：</label>
                        <select class="form-control" id="class" name="class">
                            <option>基礎班一</option>
                            <option>基礎班二</option>
                            <option>本科班一</option>
                            <option>本科班二</option>
                            <option>本科班三</option>
                            <option>專科班一</option>
                            <option>專科班二</option>
                            <option>專科班三</option>
                            <option>專科班四</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="start">起始日起：</label>
                        <input type="date" class="form-control" id="start">
                    </div>
                    <div class="form-group">
                        <label for="date_length">長度：</label>
                        <select class="form-control" id="sel1">
                            <option>一年份</option>
                            <option>二年份</option>
                            <option>一個月</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="paragraph">每個課程段落後念佛Ｘ日：</label>
                        <input type="number" class="form-control" id="paragraph">
                    </div>
                    <div class="form-group">
                        <label for="pwd">遇到排念佛日：</label>
                        <select class="form-control" id="sel1">
                            <option>星期日</option>
                            <option>星期一</option>
                            <option>星期二</option>
                            <option>星期三</option>
                            <option>星期四</option>
                            <option>星期五</option>
                            <option>星期六</option>
                        </select>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-success">送出</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

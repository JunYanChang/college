@foreach($data as $v)
<!-- The Modal -->
<div class="modal" id="edit{{ $v->id }}">
    <div class="modal-dialog modal-lg">
        <form class="modal-content" action="{{ route('forum_theme_edit',['id'=>$v->id]) }}" method="post">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">修改主題名稱</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">名稱:</label>
                        <input type="text" class="form-control" id="name" name="name" value="{{ $v->name }}">
                    </div>
                </div>
                {{ csrf_field() }}
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">送出</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </form>
    </div>
</div>
@endforeach

<!-- The Modal -->
<div class="modal" id="new">
    <div class="modal-dialog ">
        <form class="modal-content" method="post" action="{{ route('forum_theme_add') }}">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">新增主題</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
                <!-- Modal body -->
                <div class="modal-body">
                    <div class="form-group">
                        <label for="name">名稱:</label>
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>
                {{ csrf_field() }}

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">送出</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </form>
    </div>
</div>

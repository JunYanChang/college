<!-- The Modal -->
<div class="modal" id="edit">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title">修改公告訊息</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <form>
                <!-- Modal body -->
                <div class="modal-body">
                    <textarea id="message_edit" name="message_edit"></textarea>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">送出</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">取消</button>
                </div>
            </form>
        </div>
    </div>
</div>

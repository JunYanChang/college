@extends('layouts.layout')

@push('script')
    <script>
        $(document).ready(function () {
            $('li[name=discussion_li]').addClass('active');
            $('div[name=discussion]').addClass('show');
        });
    </script>
@endpush
@section('navbar')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">討論版管理</li>
        <li class="breadcrumb-item text_label"><b>主題編輯</b></li>
    </ol>
@endsection
@section('content')
    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-8">
                    <h2>討論版主題</h2>
                </div>
                <div class="col-4">
                    <button class="btn btn-primary" style="float: right" data-toggle="modal" data-target="#new">新增主題</button>
                </div>
                <div class="col-12 pt-3">
                    <div class="row">
                        @foreach($data as $v)
                        <div class="col-12 col-sm-6 col-md-6 col-lg-4 pb-3">
                            <div class="card bg-primary text-white">
                                <div class="card-body row">
                                    <div class="col-8">
                                        {{ $v->name }}
                                    </div>
                                    <div class="col-4" style="text-align: right">
                                        <a href="#edit{{ $v->id }}" style="color: white;font-size: 1.2rem" data-toggle="modal"><i class="fas fa-edit"></i></a>
                                        <a href="#delete{{ $v->id }}" style="color: red;font-size: 1.2rem" data-toggle="modal"><i class="fas fa-times"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
        <!-- /.container-fluid-->
        <!-- /.content-wrapper-->
    </div>
@endsection
@include('modals.forum_theme_edit')
@include('modals.forum_theme_new')
@include('modals.forum_theme_delete')

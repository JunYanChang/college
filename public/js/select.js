$(document).ready(function () {
    $('#student_data_selectAll').click(function () {
        if ($("input[name='student_data[]']").is(':checked')) {
            $("input[name='student_data[]']").prop('checked', false);
        } else {
            $("input[name='student_data[]']").prop('checked', true);
        }
    });

    $('#black_list_selectAll').click(function () {
        if ($("input[name='black_list[]']").is(':checked')) {
            $("input[name='black_list[]']").prop('checked', false);
        } else {
            $("input[name='black_list[]']").prop('checked', true);
        }
    });

    $('#drop_out_selectAll').click(function () {
        if ($("input[name='drop_out[]']").is(':checked')) {
            $("input[name='drop_out[]']").prop('checked', false);
        } else {
            $("input[name='drop_out[]']").prop('checked', true);
        }
    });

    $('#drop_in_selectAll').click(function () {
        if ($("input[name='drop_in[]']").is(':checked')) {
            $("input[name='drop_in[]']").prop('checked', false);
        } else {
            $("input[name='drop_in[]']").prop('checked', true);
        }
    });

    $('#recode_selectAll').click(function () {
        if ($("input[name='recode[]']").is(':checked')) {
            $("input[name='recode[]']").prop('checked', false);
        } else {
            $("input[name='recode[]']").prop('checked', true);
        }
    });

    $('#course_selectAll').click(function () {
        if ($("input[name='course[]']").is(':checked')) {
            $("input[name='course[]']").prop('checked', false);
        } else {
            $("input[name='course[]']").prop('checked', true);
        }
    });

    $('#course_level_selectAll').click(function () {
        if ($("input[name='course_level[]']").is(':checked')) {
            $("input[name='course_level[]']").prop('checked', false);
        } else {
            $("input[name='course_level[]']").prop('checked', true);
        }
    });

    $('#course_data_selectAll').click(function () {
        if ($("input[name='course_data[]']").is(':checked')) {
            $("input[name='course_data[]']").prop('checked', false);
        } else {
            $("input[name='course_data[]']").prop('checked', true);
        }
    });

    $('#subject_class_selectAll').click(function () {
        if ($("input[name='subject_class[]']").is(':checked')) {
            $("input[name='subject_class[]']").prop('checked', false);
        } else {
            $("input[name='subject_class[]']").prop('checked', true);
        }
    });

    $('#sup_elective_selectAll').click(function () {
        if ($("input[name='sup_elective[]']").is(':checked')) {
            $("input[name='sup_elective[]']").prop('checked', false);
        } else {
            $("input[name='sup_elective[]']").prop('checked', true);
        }
    });

    $('#work_grade_selectAll').click(function () {
        if ($("input[name='work_grade[]']").is(':checked')) {
            $("input[name='work_grade[]']").prop('checked', false);
        } else {
            $("input[name='work_grade[]']").prop('checked', true);
        }
    });

    $('#students_grade_selectAll').click(function () {
        if ($("input[name='students_grade[]']").is(':checked')) {
            $("input[name='students_grade[]']").prop('checked', false);
        } else {
            $("input[name='students_grade[]']").prop('checked', true);
        }
    });

    $('#de_sing_selectAll').click(function () {
        if ($("input[name='de_sing[]']").is(':checked')) {
            $("input[name='de_sing[]']").prop('checked', false);
        } else {
            $("input[name='de_sing[]']").prop('checked', true);
        }
    });

    $('#students_update_selectAll').click(function () {
        if ($("input[name='students_update[]']").is(':checked')) {
            $("input[name='students_update[]']").prop('checked', false);
        } else {
            $("input[name='students_update[]']").prop('checked', true);
        }
    });

    $('#recode_update_selectAll').click(function () {
        if ($("input[name='recode_update[]']").is(':checked')) {
            $("input[name='recode_update[]']").prop('checked', false);
        } else {
            $("input[name='recode_update[]']").prop('checked', true);
        }
    });

    $('#gift_update_selectAll').click(function () {
        if ($("input[name='gift_update[]']").is(':checked')) {
            $("input[name='gift_update[]']").prop('checked', false);
        } else {
            $("input[name='gift_update[]']").prop('checked', true);
        }
    });

    $('#bulletin_selectAll').click(function () {
        if ($("input[name='bulletin[]']").is(':checked')) {
            $("input[name='bulletin[]']").prop('checked', false);
        } else {
            $("input[name='bulletin[]']").prop('checked', true);
        }
    });

    $('#drop_mail_selectAll').click(function () {
        if ($("input[name='drop_mail[]']").is(':checked')) {
            $("input[name='drop_mail[]']").prop('checked', false);
        } else {
            $("input[name='drop_mail[]']").prop('checked', true);
        }
    });

    $('#update_mail_selectAll').click(function () {
        if ($("input[name='update_mail[]']").is(':checked')) {
            $("input[name='update_mail[]']").prop('checked', false);
        } else {
            $("input[name='update_mail[]']").prop('checked', true);
        }
    });

    $('#enrollment_selectAll').click(function () {
        if ($("input[name='enrollment[]']").is(':checked')) {
            $("input[name='enrollment[]']").prop('checked', false);
        } else {
            $("input[name='enrollment[]']").prop('checked', true);
        }
    });

    $('#guidance_selectAll').click(function () {
        if ($("input[name='guidance[]']").is(':checked')) {
            $("input[name='guidance[]']").prop('checked', false);
        } else {
            $("input[name='guidance[]']").prop('checked', true);
        }
    });

    $('#plan_selectAll').click(function () {
        if ($("input[name='plan[]']").is(':checked')) {
            $("input[name='plan[]']").prop('checked', false);
        } else {
            $("input[name='plan[]']").prop('checked', true);
        }
    });

    $('#understanding_selectAll').click(function () {
        if ($("input[name='understanding[]']").is(':checked')) {
            $("input[name='understanding[]']").prop('checked', false);
        } else {
            $("input[name='understanding[]']").prop('checked', true);
        }
    });

    $('#calendar_selectAll').click(function () {
        if ($("input[name='calendar[]']").is(':checked')) {
            $("input[name='calendar[]']").prop('checked', false);
        } else {
            $("input[name='calendar[]']").prop('checked', true);
        }
    });

    $('#announcement_selectAll').click(function () {
        if ($("input[name='announcement[]']").is(':checked')) {
            $("input[name='announcement[]']").prop('checked', false);
        } else {
            $("input[name='announcement[]']").prop('checked', true);
        }
    });

    $('#scripture_selectAll').click(function () {
        if ($("input[name='scripture[]']").is(':checked')) {
            $("input[name='scripture[]']").prop('checked', false);
        } else {
            $("input[name='scripture[]']").prop('checked', true);
        }
    });

    $('#teaching_material_selectAll').click(function () {
        if ($("input[name='teaching_material[]']").is(':checked')) {
            $("input[name='teaching_material[]']").prop('checked', false);
        } else {
            $("input[name='teaching_material[]']").prop('checked', true);
        }
    });

    $('#role_selectAll').click(function () {
        if ($("input[name='role[]']").is(':checked')) {
            $("input[name='role[]']").prop('checked', false);
        } else {
            $("input[name='role[]']").prop('checked', true);
        }
    });

    $('#account_selectAll').click(function () {
        if ($("input[name='account[]']").is(':checked')) {
            $("input[name='account[]']").prop('checked', false);
        } else {
            $("input[name='account[]']").prop('checked', true);
        }
    });

    $('#class_level_1_selectAll').click(function () {
        if ($("input[name='class_course_1[]']").is(':checked')) {
            $("input[name='class_course_1[]']").prop('checked', false);
        } else {
            $("input[name='class_course_1[]']").prop('checked', true);
        }
    });

    $('#class_level_2_selectAll').click(function () {
        if ($("input[name='class_course_2[]']").is(':checked')) {
            $("input[name='class_course_2[]']").prop('checked', false);
        } else {
            $("input[name='class_course_2[]']").prop('checked', true);
        }
    });
    $('#class_level_3_selectAll').click(function () {
        if ($("input[name='class_course_3[]']").is(':checked')) {
            $("input[name='class_course_3[]']").prop('checked', false);
        } else {
            $("input[name='class_course_3[]']").prop('checked', true);
        }
    });
    $('#class_level_4_selectAll').click(function () {
        if ($("input[name='class_course_4[]']").is(':checked')) {
            $("input[name='class_course_4[]']").prop('checked', false);
        } else {
            $("input[name='class_course_4[]']").prop('checked', true);
        }
    });
    $('#class_level_5_selectAll').click(function () {
        if ($("input[name='class_course_5[]']").is(':checked')) {
            $("input[name='class_course_5[]']").prop('checked', false);
        } else {
            $("input[name='class_course_5[]']").prop('checked', true);
        }
    });
    $('#class_level_6_selectAll').click(function () {
        if ($("input[name='class_course_6[]']").is(':checked')) {
            $("input[name='class_course_6[]']").prop('checked', false);
        } else {
            $("input[name='class_course_6[]']").prop('checked', true);
        }
    });
    $('#class_level_7_selectAll').click(function () {
        if ($("input[name='class_course_7[]']").is(':checked')) {
            $("input[name='class_course_7[]']").prop('checked', false);
        } else {
            $("input[name='class_course_7[]']").prop('checked', true);
        }
    });
    $('#class_level_8_selectAll').click(function () {
        if ($("input[name='class_course_8[]']").is(':checked')) {
            $("input[name='class_course_8[]']").prop('checked', false);
        } else {
            $("input[name='class_course_8[]']").prop('checked', true);
        }
    });
    $('#class_level_9_selectAll').click(function () {
        if ($("input[name='class_course_9[]']").is(':checked')) {
            $("input[name='class_course_9[]']").prop('checked', false);
        } else {
            $("input[name='class_course_9[]']").prop('checked', true);
        }
    });
});

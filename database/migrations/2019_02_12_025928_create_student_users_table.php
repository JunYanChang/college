<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
/** 學生個人資料， 帳號密碼 */
class CreateStudentUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('student_id')->comment('學號');
            $table->string('name')->comment('姓名');
            $table->string('dharma_name')->nullable()->comment('法號');
            $table->enum('gender', ['男', '女']);
            $table->string('nationality')->comment('國籍');
            $table->string('email')->comment('E-mail');
            $table->string('phone')->nullable()->comment('電話');
            $table->string('cellphone')->nullable()->comment('行動電話');
            $table->date('birthday')->nullable()->comment('生日');
            $table->string('address')->nullable()->comment('地址');
            $table->string('language')->nullable()->comment('語言');
            $table->string('fax')->nullable()->comment('傳真');
            $table->string('job')->nullable()->comment('職業');
            $table->string('skill')->nullable()->comment('技能');
            $table->string('Volunteer')->nullable()->comment('義工');
            $table->string('course_level')->comment('年級');
            $table->integer('stay_in_school')->default(1)->comment('在學狀態');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_users');
    }
}

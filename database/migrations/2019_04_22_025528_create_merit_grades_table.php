<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeritGradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('merit_grades', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('merit')->comment('項目');
            $table->integer('grade')->comment('分數');
            $table->integer('yes')->comment('功的數量');
            $table->integer('no')->comment('過的數量');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merit_grades');
    }
}

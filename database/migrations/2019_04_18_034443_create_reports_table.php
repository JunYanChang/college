<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** 提交報告 */
class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student')->comment('學生');
            $table->integer('curricula')->comment('課程');
            $table->string('grade')->comment('成績');
            $table->integer('status')->comment('狀態');
            $table->mediumText('reports')->comment('報告內容');
            $table->mediumText('respond')->comment('老師回應');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}

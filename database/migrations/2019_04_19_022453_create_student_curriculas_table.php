<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** 學生選課 */
class CreateStudentCurriculasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_curriculas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('student')->comment('學生');
            $table->integer('curricula')->comment('課程');
            $table->integer('done')->comment('通過課程')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_curriculas');
    }
}

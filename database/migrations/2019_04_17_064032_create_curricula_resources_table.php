<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** 文章資源 */

class CreateCurriculaResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curricula_resources', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_data')->comment('課程');
            $table->integer('media')->comment('影音');
            $table->integer('ep')->comment('集數');
            $table->string('type')->comment('類型，pdf,word,txt');
            $table->string('attr')->comment('連結');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curricula_resources');
    }
}


<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** 課表 */
class CreateCurriculaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curricula', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->comment('班級');
            $table->integer('course_data')->comment('課程資源');
            $table->integer('report')->comment('提交報告');
            $table->integer('fraction')->comment('學分');
            $table->integer('compulsory')->comment('必修');
            $table->mediumText('remark')->comment('備註');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curricula');
    }
}

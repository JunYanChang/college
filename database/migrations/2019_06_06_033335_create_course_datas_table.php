<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sn')->comment('課程編號');
            $table->string('title')->comment('課程名稱');
            $table->integer('ep')->comment('集數');
            $table->string('teacher')->comment('授課老師');
            $table->string('separation')->comment('分集');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_datas');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/** 課程影音資源 */
class CreateCurriculaMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curricula_media', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_data')->comment('課程');
            $table->integer('ep')->comment('集數');
            $table->integer('type')->comment('類型，mp3,mp4,xml');
            $table->string('attr')->comment('連結');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curricula_media');
    }
}

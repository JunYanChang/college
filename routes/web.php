<?php

Route::prefix('admin')->group(function (){

    Route::get('/','LoginController@login')->name('login');
//    ->middleware('LoginValidate')
    Route::post('validatedData','LoginController@validatedData')->name('validatedData');
    Route::post('logout/{user}','LoginController@logout')->name('logout');

    Route::resource('dashboard','DashboardController');
//    ->middleware('ValidateUser')
    Route::prefix('students')->group(function (){
        Route::resource('data', 'StuDataController',['except'=>['create','store']]);
        Route::resource('black_list','BlackListController');
        Route::any('drop_out','StudentController@drop_out')->name('students_drop_out');
        Route::any('drop_in','StudentController@drop_in')->name('students_drop_in');
        Route::any('drop_all','StudentController@drop_all')->name('students_drop_all');
        Route::any('drop_mail_config','StudentController@drop_mail_config')->name('students_drop_mail_config');
    });

    Route::prefix('Class')->group(function (){
        Route::resource('course','CourseController');
        Route::resource('course_level','CourseLevelController',['only'=>['index','create','destroy']]);
        Route::resource('course_data','CourseDataController');
        Route::resource('subject_class','SubjectClassController');
        Route::resource('sup_elective','SupElectiveController');
    });

    Route::prefix('Grade')->group(function (){
        Route::resource('work_grade','WorkGradeController');
        Route::resource('UnReport','UnreportController',['only'=>['index']]);
        Route::post('UnReport_mail','UnreportController@mail_notice')->name('UnReport_mail');
        Route::post('UnReport_all_mail','UnreportController@all_mail_notice')->name('UnReport_all_mail');
        Route::resource('students_grade','StudentGradeController');
        Route::get('de_sing','GradeController@de_sing')->name('de_sing');
        Route::get('de_sing_list_grade','GradeController@de_sing_list_grade')->name('de_sing_list_grade');
    });

    Route::prefix('Update')->group(function (){
        Route::resource('students_update','StudentsUpdateController');
        Route::post('all_update','StudentsUpdateController@all_update')->name('update');
        Route::post('exception_update/{id}','StudentsUpdateController@exception_update')->name('exception_update');
        Route::post('keyin_exception_update','StudentsUpdateController@keyin_exception_update')->name('keyin_exception_update');
        Route::resource('recode_update','UpdateRecodeController');
    });

//    Route::prefix('Forum')->group(function (){
//        Route::prefix('theme')->group(function (){
//            Route::any('theme','ForumController@theme')->name('forum_theme');
//            Route::post('/add','ForumController@theme_add')->name('forum_theme_add');
//            Route::post('/edit/{id}','ForumController@theme_edit')->name('forum_theme_edit');
//            Route::post('/delete/{id}','ForumController@theme_delete')->name('forum_theme_delete');
//        });
//    });

    Route::prefix('announcement')->group(function (){
        Route::resource('bulletin','BulletinController');
        Route::post('create_type','BulletinController@create_type')->name('create_type');
        Route::delete('delete_relies/{replies}','BulletinController@delete_relies')->name('delete_relies');
    });

    Route::prefix('email_config')->group(function (){
        Route::prefix('drop')->group(function (){
            Route::any('/','EmailController@drop_show')->name('email_config_drop');
            Route::post('out/{type}','EmailController@email_config')->name('email_config_dropout');
            Route::post('in/{type}','EmailController@email_config')->name('email_config_dropin');
        });
        Route::prefix('update')->group(function (){
            Route::any('/','EmailController@update_show')->name('email_config_update');
            Route::post('save/{type}','EmailController@email_config')->name('email_config_update_save');
        });
        Route::prefix('enrollment')->group(function (){
           Route::any('/','EmailController@enrollment_show')->name('email_config_enrollment');
           Route::post('save/{type}','EmailController@email_config')->name('email_config_enrollment_save');
        });
    });

    Route::prefix('Edit')->group(function (){
        //入學指導
        Route::prefix('guidance')->group(function (){
            Route::any('/','EditController@admission_guidance')->name('edit_admission_guidance');
            Route::post('save/{type}','EditController@edit')->name('edit_admission_guidance_save');
        });
        //教學計畫
        Route::prefix('plan')->group(function (){
            Route::any('/','EditController@plan')->name('edit_plan');
            Route::post('save/{type}','EditController@edit')->name('edit_plan_save');
        });
        //認識學院
        Route::prefix('understanding')->group(function (){
            Route::any('/','EditController@understanding')->name('edit_understanding');
            Route::post('save/{type}','EditController@edit')->name('edit_understanding_save');
        });
        //年度行事曆
        Route::prefix('calendar')->group(function (){
            Route::any('/','EditCalendarController@calendar')->name('edit_calendar');
            Route::post('/save/{year}/{month}','EditCalendarController@calendar_save')->name('calendar_save');
        });
        //說明看板
        Route::prefix('description')->group(function (){
            Route::any('/','EditController@description')->name('edit_description');
            Route::post('save/{type}','EditController@edit')->name('edit_description_save');
        });
        //訊息公告
        Route::prefix('announcement')->group(function (){
            Route::any('/','EditController@announcement')->name('edit_announcement');
            Route::post('/mange/{config}','EditController@announcement_mange')->name('edit_announcement_mange');
            Route::post('/save/{type}','EditController@edit')->name('edit_announcement_save');
        });
    });

    Route::prefix('File')->group(function (){
        Route::resource('scripture','FileScriptureController');
        Route::resource('teaching_material','FileTeachingMaterial');
    });

    Route::prefix('administration')->group(function (){
        Route::resource('role','RoleController');
        Route::resource('account','AccountController');
    });
});

Route::any('test','TestController@test');
//Route::any('testshow','TestController@testshow');

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class course_data extends Model
{
    protected $guarded = [];

    public function get_media(){
        return $this->hasMany('App\Curricula_media','course_data');
    }
}

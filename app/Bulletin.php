<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin extends Model
{
    protected $guarded = [];

    public function get_replies(){
        return $this->hasMany('App\Bulletin_replies','bulletin');
    }

    public function get_type(){
        return $this->belongsTo('App\Bulletin_type','type');
    }
}

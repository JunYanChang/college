<?php

namespace App\Imports;

use App\Student_user;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class UsersImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $row)
        {
            Student_user::insert([
                'student_id' => $row[1],
                'name' => $row[2],
                'gender' => $row[3],
                'nationality' => $row[4],
                'email' => $row[5],
                'course_level' => 1,
                'stay_in_school' => 1
            ]);
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin_type extends Model
{
    protected $guarded = [];

    public function get_bulletin(){
        return $this->hasMany('App\Bulletin','type');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blacklist extends Model
{
    protected $guarded = [];

    public function get_student(){
        return $this->belongsTo('App\Student_user','student');
    }
}

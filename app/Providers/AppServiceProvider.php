<?php

namespace App\Providers;

use App\AdminUser;
use App\Course;
use App\course_data;
use App\Course_level;
use App\Curriculum;
use App\Student_user;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('check_student_id', function ($attribute, $value, $parameters, $validator) {
            return Student_user::where('student_id',$value)->count() == 1;
        });

        Validator::extend('check_course_name',function($attribute, $value, $parameters, $validator){
            return Course::where('name',$value)->count() == 0;
        });

        Validator::extend('check_sn',function($attribute, $value, $parameters, $validator){
           return Course_data::where('sn',$value)->count() == 0;
        });

        Validator::extend('check_level_code',function($attribute, $value, $parameters, $validator){
           return Course_level::where('code',substr($value,0,2))->count() == 1 && Student_user::where('student_id',$value)->count() == 0;
        });

        Validator::extend('check_account',function($attribute, $value, $parameters, $validator){
           return AdminUser::where('account',$value)->count() != 0;
        });

        Validator::extend('check_login',function($attribute, $value, $parameters, $validator){
           return AdminUser::where('account',$value)->count() == 1;
        });

        view()->composer('*',function($view){
           $view->with('user',Session::get('user'));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}


<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $guarded = [];

    public function student_id(){
        return $this->belongsTo('App\Student_user','student');
    }

    public function course(){
        return $this->belongsTo('App\Curriculum','curricula');
    }
}

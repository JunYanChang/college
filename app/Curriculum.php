<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curriculum extends Model
{
    protected $guarded = [];

    public function coursedata(){
        return $this->belongsTo('App\Course_data','course_data');
    }

    public function student_curricula(){
        return $this->hasMany('App\Student_curricula','curricula');
    }
}

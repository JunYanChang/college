<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_recode extends Model
{
    public function get_student(){
        return $this->belongsTo('App\Student_user','student');
    }

    public function get_level(){
        return $this->belongsTo('App\Course_level','old_level');
    }
}

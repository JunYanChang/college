<?php

namespace App\Policies;

use App\AdminUser;
use App\User;
use App\Student_user;
use Illuminate\Auth\Access\HandlesAuthorization;

class Policy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the student_user.
     *
     * @param  \App\User  $user
     * @param  \App\Student_user  $studentUser
     * @return mixed
     */
    public function view(AdminUser $user, Student_user $studentUser)
    {
        if(strpos($user->role_name->function,'data-view') !== false ){
               return true;
           }
           return false;
    }

    /**
     * Determine whether the user can create student_users.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(AdminUser $user)
    {
        return in_array($user->account,[
            'admin',
        ]);
    }

    /**
     * Determine whether the user can update the student_user.
     *
     * @param  \App\User  $user
     * @param  \App\Student_user  $studentUser
     * @return mixed
     */
    public function update(User $user, Student_user $studentUser)
    {
        //
    }

    /**
     * Determine whether the user can delete the student_user.
     *
     * @param  \App\User  $user
     * @param  \App\Student_user  $studentUser
     * @return mixed
     */
    public function delete(User $user, Student_user $studentUser)
    {
        //
    }

    /**
     * Determine whether the user can restore the student_user.
     *
     * @param  \App\User  $user
     * @param  \App\Student_user  $studentUser
     * @return mixed
     */
    public function restore(User $user, Student_user $studentUser)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the student_user.
     *
     * @param  \App\User  $user
     * @param  \App\Student_user  $studentUser
     * @return mixed
     */
    public function forceDelete(User $user, Student_user $studentUser)
    {
        //
    }
}

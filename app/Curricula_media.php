<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Curricula_media extends Model
{
    public function data()
    {
        return $this->belongsTo('App\course_data','course_data');
    }

    public function resource(){
        return $this->hasMany('App\Curricula_resource','media');
    }
}

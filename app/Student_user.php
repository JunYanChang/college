<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_user extends Model
{
    protected $guarded = [];

    public function level(){
        return $this->belongsTo('App\Course_level','course_level');
    }

    public function curriculas(){
        return $this->hasMany('App\Student_curricula','student');
    }

    public function report(){
        return $this->hasMany('App\Report','student');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bulletin_replies extends Model
{
    public function get_student(){
        return $this->belongsTo('App\Student_user','student');
    }

    public function get_bulletin(){
        return $this->belongsTo('App\Bulletin','bulletin');
    }
}

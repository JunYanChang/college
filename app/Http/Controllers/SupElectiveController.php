<?php

namespace App\Http\Controllers;

use App\Config;
use App\Student_curricula;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SupElectiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $search = $request->query('search','');
            $config = Config::where('title','elective')->first();
            $data = Student_user::where('student_id',$search)->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('sup_elective_course',compact('config','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::transaction(function ()use($request){
                Student_curricula::create(['student'=>$request->student,'curricula'=>$request->id]);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Config $sup_elective
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Config $sup_elective)
    {
        try{
            DB::transaction(function ()use($request,$sup_elective){
                $sup_elective->update(['config'=>$request->config]);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Student_curricula $sup_elective
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Student_curricula $sup_elective)
    {
        try{
            DB::transaction(function ()use($sup_elective){
                $sup_elective->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

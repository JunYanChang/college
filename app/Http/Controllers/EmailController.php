<?php

namespace App\Http\Controllers;

use App\Mail_notice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EmailController extends Controller
{
    public function drop_show(){
        try{
            $dropout = Mail_notice::where('type','dropout_email_notice')->first();
            $dropin = Mail_notice::where('type','dropin_email_notice')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,"頁面資訊錯誤！");
        }

        return view('admin.MailConfig.mail_config_drop',compact('dropin','dropout'));
    }

    public function update_show(){
        try{
            $data = Mail_notice::where('type','update_email_notice')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,"頁面資訊錯誤！");
        }
        return view('admin.MailConfig.mail_config_update',compact('data'));
    }

    public function enrollment_show(){
        try{
            $data = Mail_notice::where('type','enrollment_email_notice')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,"頁面資訊錯誤！");
        }
        return view('admin.MailConfig.mail_config_enrollment',compact('data'));
    }

    public function email_config(Request $request,$type){
        try{
            $data = Mail_notice::where('type',$type)->first();
            if(empty($data)){
                $insert = new Mail_notice;
                $insert->type = $type;
                $insert->content = $request->input('content');
                $insert->save();
            }else{
                $validatedData = $request->validate([
                    'content' => 'required',
                ]);
                $data->update($validatedData);
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }
}

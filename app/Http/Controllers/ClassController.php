<?php

namespace App\Http\Controllers;

use App\Classes\toChineseNumber;
use App\Classes\txtRead;
use App\Config;
use App\Course;
use App\course_data;
use App\Course_level;
use App\course_resource_type;
use App\Curricula_media;
use App\Curricula_resource;
use App\Curriculum;
use App\Host;
use App\Student_curricula;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ClassController extends Controller
{
    //課程 CRUD
    public function course(){
        try{
            $data = Course::all();
            return view('course',['data'=>$data]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
    }

    public function course_create(Request $request){
        if($request->isMethod('get')){
            try{
                return view('course_create');
            }catch(\Exception $e){
                Log::info($e->getMessage());
                abort(403,'頁面錯誤.');
            }
        }else {
            try{
                $course = new Course();
                $course->name = $request->name;
                $course->intro = $request->intro;
                $course->save();
                DB::commit();

                return redirect()->route('course');
            }catch (\Exception $e){
                Log::info($e->getMessage());
                DB::rollBack();

                return redirect()->route('course');
            }
        }
    }

    public function course_edit($id,Request $request){
        if($request->isMethod('get')){
            try{
                $data = Course::where('id',$id)->first();
                return view('course_edit',['data'=>$data]);
            }catch (\Exception $e){
                Log::info($e->getMessage());
                abort(403,'頁面錯誤.');
            }
        }else{
            try{
                $course = Course::find($id);
                $course->name = $request->name;
                $course->intro = $request->intro;
                $course->save();
                DB::commit();

                return redirect()->route('course');
            }catch (\Exception $e){
                Log::info($e->getMessage());
                DB::rollBack();

                return redirect()->route('course');
            }
        }
    }

    public function course_delete($id){
        try{
            $data = Course::find($id);
            $data->delete();
            DB::commit();

            return back();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();

            return back();
        }
    }

    //班級 CRUD
    public function course_level(){
        try{
            $data = Course::all();
            return view('course_level',['data'=>$data]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
    }

    public function course_level_create($id){
        try{
            $num2Zh = new toChineseNumber();
            switch (Course::find($id)->name){
                case "基礎學科":
                    $level = "基礎班".$num2Zh->num2zh(Course_level::where('courses',$id)->count() + 1)."年級";
                    break;
                case "本科學科":
                    $level = "本科班".$num2Zh->num2zh(Course_level::where('courses',$id)->count() + 1)."年級";
                    break;
                case "專科學科":
                    $level = "專科班".$num2Zh->num2zh(Course_level::where('courses',$id)->count() + 1)."年級";
                    break;

            }
            $Course_level = new Course_level();
            $Course_level->courses = $id;
            $Course_level->level = $level;
            $Course_level->save();
            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());

            return back();
        }
    }

    public function course_level_delete($id){
        try{
            $latest_id = Course_level::where('courses',$id)->latest()->first()->id;
            Course_level::destroy($latest_id);
            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());

            return back();
        }
    }

    //影音資料庫 CRUD
    //read
    public function course_data(Request $request){
        try{
            $data = course_data::all();
            if($request->has('search')){
                $s = $request->search;
                $data = course_data::orwhere('sn','like','%'.$s.'%')->orwhere('title','like','%'.$s.'%')->orwhere('teacher','like','%'.$s.'%')->get();
            }
            return view('course_data',['data'=>$data]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
    }

    //create
    public function course_data_create(Request $request){
        try{
            DB::transaction(function ()use($request){
                $data = new course_data();
                $data->sn = $request->course_id;
                $data->title = $request->course_name;
                $data->teacher = $request->teacher;
                $data->separation = $request->separation;
                $data->ep = $request->end_ep - $request->start_ep + 1;
                $data->start_ep = $request->start_ep;
                $data->end_ep = $request->end_ep;
                $data->type = $request->type;
                $data->save();

                $sn = explode("-",$request->course_id);

                for($i=$request->start_ep;$i<=$request->end_ep;$i++){
                    $media = new Curricula_media();
                    $media->course_data = $data->id;
                    $media->ep = $i;
                    $media->attr = "/media/".$request->type."/".$sn[0]."/".$request->course_id."/".$request->course_id."-".str_pad($i,4,'0',STR_PAD_LEFT).$request->separation.".".$request->type;
                    $media->save();

                    for($j=1;$j<=3;$j++){
                        $resource_txt = new Curricula_resource();
                        $resource_txt->course_data = $data->id;
                        $resource_txt->media = $media->id;
                        $resource_txt->ep = $i;
                        $resource_txt->type = course_resource_type::find($j)->type;
                        $resource_txt->attr = "http://ft.hwadzan.com/ft.php?sn=".$request->course_id."-".str_pad($i,4,'0',STR_PAD_LEFT)."&docstype=".course_resource_type::find($j)->type."&lang=zh_TW";;
                        $resource_txt->save();
                    }
                }
            });
            DB::commit();
            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    //update
    public function course_data_edit($id,Request $request){
        try {
            DB::transaction(function ()use($id,$request){
                $sn = explode("-",$request->course_id);
                $old_ep = course_data::where('id',$id)->first()->end_ep;
                if($request->end_ep < $old_ep){
                    Curricula_media::where('course_data', $id)->whereBetween('ep', [($request->end_ep+1),$old_ep])->delete();
                    Curricula_resource::where('course_data', $id)->whereBetween('ep', [($request->end_ep+1),$old_ep])->delete();
                }elseif ($request->end_ep > $old_ep){
                    for($i = ($old_ep+1);$i<=$request->end_ep;$i++){
                        $media = new Curricula_media();
                        $media->course_data = $id;
                        $media->ep = $i;
                        $media->attr = "/media/".$request->type."/".$sn[0]."/".$request->course_id."/".$request->course_id."-".str_pad($i,4,'0',STR_PAD_LEFT).$request->separation.".".$request->type;
                        $media->save();

                        for ($j = 1; $j <= 3; $j++) {
                            $resource_txt = new Curricula_resource();
                            $resource_txt->course_data = $id;
                            $resource_txt->media = $media->id;
                            $resource_txt->ep = $i;
                            $resource_txt->type = course_resource_type::find($j)->type;
                            $resource_txt->attr = "http://ft.hwadzan.com/ft.php?sn=" . $request->course_id . "-" . str_pad($i, 4, '0', STR_PAD_LEFT) . "&docstype=" . course_resource_type::find($j)->type . "&lang=zh_TW";;
                            $resource_txt->save();
                        }
                    }
                }

                $Course_data = course_data::find($id);
                $Course_data->sn = $request->course_id;
                $Course_data->title = $request->course_name;
                $Course_data->teacher = $request->teacher;
                $Course_data->separation = $request->separation;
                $Course_data->ep = $request->end_ep - $request->start_ep + 1;
                $Course_data->start_ep = $request->start_ep;
                $Course_data->end_ep = $request->end_ep;
                $Course_data->type = $request->type;
                $Course_data->save();

                //修改影音文檔連結
                for($i=$request->start_ep;$i<=$request->end_ep;$i++){
                    $media_u = Curricula_media::where('course_data',$id)->where('ep',$i)->first();
                    $media_u->attr = "/media/".$request->type."/".$sn[0]."/".$request->course_id."/".$request->course_id."-".str_pad($i,4,'0',STR_PAD_LEFT).$request->separation.".".$request->type;
                    $media_u->save();

                    for ($j = 1; $j <= 3; $j++) {
                        $resource_txt = Curricula_resource::where('course_data',$id)->where('ep',$i)->where('type',course_resource_type::find($j)->type)->first();
                        $resource_txt->attr = "http://ft.hwadzan.com/ft.php?sn=" . $request->course_id . "-" . str_pad($i, 4, '0', STR_PAD_LEFT) . "&docstype=" . course_resource_type::find($j)->type . "&lang=zh_TW";;
                        $resource_txt->save();
                    }
                }
            });
            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    //delete
    public function course_data_delete($id){
        try{
            DB::transaction(function ()use($id){
                Curricula_media::where('course_data',$id)->delete();
                Curricula_resource::where('course_data',$id)->delete();
                course_data::destroy($id);
            });

            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    //影音
    public function course_media($id,Request $request){
        try{
            $data = Curricula_media::where('course_data',$id)->paginate(6);
            $host = Host::find(1)->attr;
            if($request->has('host')){
                $host = Host::find($request->host)->attr;
            }
            $txt = new txtRead();
            return view('course_media',['data'=>$data,'txt'=>$txt,'host'=>$host,'id'=>$id]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }

    }

    //課表與學分編輯 CRUD
    //todo:移除學分項目
    //read
    public function subject_class($id){
        try{
            $level = Course_level::all();
            $data = Course_level::find($id);
            $course = course_data::all();
            return view('subject_class',['level'=>$level,'data'=>$data,'course'=>$course,'id'=>$id]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
    }
    //create
    public function subject_class_create($id,Request $request){
        try{
            $data = new Curriculum();
            $data->level = $id;
            $data->course_data = $request->class;
            $data->report = $request->class_homework;
            $data->fraction = $request->credit;
            $data->compulsory = $request->class_type;
            $data->remark = $request->remark;
            $data->save();

            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }

    }
    //update
    public function subject_class_edit($id,Request $request){
        try{
            $data = Curriculum::find($id);
            $data->course_data = $request->class;
            $data->report = $request->class_homework;
            $data->fraction = $request->credit;
            $data->compulsory = $request->class_type;
            $data->remark = $request->remark;
            $data->save();

            DB::commit();

            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }
    //delete
    public function subject_class_delete($id){
        try{
            Curriculum::destroy($id);
            DB::commit();
            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    //選課管理
    public function sup_elective_course(Request $request){
        try{
            $config = Config::where('title','elective')->first();
            $data = Student_user::where('student_id',$request->search)->first();
            return view('sup_elective_course',['config'=>$config,'data'=>$data]);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
    }

    public function sup_elective_config(Request $request){
        try{
           $update = Config::where('title','elective')->first();
           $update->config = $request->config;
           $update->save();
           DB::commit();
           return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    public function sup_elective_add($id,$student){
        try{
            $data = new Student_curricula();
            $data->student = Student_user::where('student_id',$student)->first()->id;
            $data->curricula = $id;
            $data->done = 0;
            $data->save();
            DB::commit();
            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }

    public function sup_elective_remove($id){
        try{
            Student_curricula::destroy($id);
            DB::commit();
            return back();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
            return back();
        }
    }
}


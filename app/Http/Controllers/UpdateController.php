<?php

namespace App\Http\Controllers;

use App\Course_level;
use App\Curriculum;
use App\Mail_notice;
use App\Student_curricula;
use App\Student_recode;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UpdateController extends Controller
{
    public function students_update($id = ""){
        try {
            $level = Course_level::all();
            $curricula = Curriculum::where([['level',$id],['report',1]])->orderBy('compulsory','asc')->get();
        }catch (\Exception $e) {
            Log::info($e->getMessage());
            abort(403,'頁面錯誤！');
        }
        return view('students_update',['level'=>$level,'curricula'=>$curricula,'id' => $id]);
    }

    public function students_update_filter($level,$status = 1,Request $request){
        try{
            $selected = $request->selected;
            $list = Student_user::whereHas('curriculas',function ($query)use($selected,$status){
                $query->whereIn('curricula',$selected)->where('done',$status);
            })->paginate(30);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            return back();
        }
        return view('students_update_filter',['list'=>$list,'level'=>$level,'status'=>$status]);
    }

    public function update(Request $request){
        try{
            DB::transaction(function ()use($request){
                $level = Course_level::count();
                foreach ($request->update_list as $l){
                    $data = Student_user::find($l);
                    //紀錄舊級別與學號
                    $old_level = $data->course_level;
                    $old_student_id = $data->student_id;
                    //判斷符合在年級範圍裡
                    if($data->course_level <= $level){
                        //取得新年級的code
                        $new_level = Course_level::find($data->course_level+1);
                        //修改學生年級與學號
                        $data->student_id = str_replace($data->level->code,$new_level->code,$data->student_id);
                        $data->course_level = $data->course_level+1;
                        $data->save();

                        $curriculas = Curriculum::where([['level',$new_level->id],['compulsory',1]])->get();
                        foreach ($curriculas as $c){
                            $student_curriculas = new Student_curricula;
                            $student_curriculas->student = $data->id;
                            $student_curriculas->curricula = $c->id;
                            $student_curriculas->save();
                        }

                    }
                    //新增升級紀錄
                    $recode = new Student_recode;
                    $recode->student = $l;
                    $recode->old_level = $old_level;
                    $recode->old_student_id = $old_student_id;
                    $recode->save();
                }
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function exception_update($id,Request $request){
        try{
            DB::transaction(function ()use($id,$request) {
                $level = Course_level::count();
                $data = Student_user::find($id);
                //紀錄舊級別與學號
                $old_level = $data->course_level;
                $old_student_id = $data->student_id;
                //判斷升降級
                switch ($request->optradio) {
                    case "update":
                        //判斷符合在年級範圍裡
                        if ($data->course_level <= $level) {
                            //取得新年級的code
                            $new_level = Course_level::find($data->course_level+1);
                            //修改學生年級與學號
                            $data->student_id = str_replace($data->level->code,$new_level->code,$data->student_id);
                            $data->course_level = $data->course_level + 1;
                            $data->save();
                        }
                        break;
                    case "downgrade":
                        //todo:降級問題解決課程選課
                        //判斷符合在年級範圍裡
                        if ($data->course_level > 1) {
                            //取得新年級的code
                            $new_level = Course_level::find($data->course_level-1);
                            //修改學生年級與學號
                            $data->student_id = str_replace($data->level->code,$new_level->code,$data->student_id);
                            $data->course_level = $data->course_level - 1;
                            $data->save();
                        }
                        break;
                }
                //todo:降級是否也要記錄
                $recode = new Student_recode;
                $recode->student = $id;
                $recode->old_level = $old_level;
                $recode->old_student_id = $old_student_id;
                $recode->remark = "例外升級";
                $recode->save();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
            return back();
        }
        return back();
    }

    //todo:寄發Email＆升級贈品網址
    public function keyin_exception_update(Request $request){
        try{
            $check = Student_user::where('student_id',$request->old_id)->count();
            $check_level_code = Course_level::where('code',substr($request->new_id,0,2))->first();

            if($check == 1 && !empty($check_level_code)){
                DB::transaction(function ()use($request,$check_level_code){
                    $data = Student_user::where('student_id', $request->old_id)->first();

                    $recode = new Student_recode;
                    $data->student_id = $request->new_id;
                    $data->course_level = $check_level_code->id;
                    $data->save();

                    $recode->student = $data->id;
                    $recode->old_level = $data->course_level;
                    $recode->old_student_id = $request->old_id;
                    $recode->remark = "例外升級";
                    $recode->save();
                });
                DB::commit();
            }else{
                return back();
            }
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }

        return back();
    }

    public function recode_update(Request $request){
        try{
            $data = Student_recode::whereHas('get_student',function ($query)use($request){
                $query->where('student_id','like','%'.$request->search.'%');
            })->paginate(30);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤');
        }
        return view('students_update_record',['data' => $data]);
    }

    public function recode_update_delete($id){
        try{
            Student_recode::destroy($id);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }



}

<?php

namespace App\Http\Controllers;

use App\Blacklist;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BlackListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = Blacklist::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.BlackList.students_black_list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validatedData = $request->validate([
                'student' => 'required|check_student_id'
            ]);

            $check = Student_user::where('student_id',$request->student)->firstOrFail();
            $data = new Blacklist;
            $data->student = $check->id;
            $data->save();

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Blacklist $black_list
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Blacklist $black_list)
    {
        try{
            DB::transaction(function ()use($black_list){
                $black_list->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

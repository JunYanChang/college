<?php

namespace App\Http\Controllers;

use App\Blacklist;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class StudentController extends Controller
{
    public function students_data(Request $request){
        try{
            $request->flash();
            $Number_pen = $request->filled('Number_pen')? $request->Number_pen : 15;
            $search = $request->filled('search') ? $request->search : "";
            $data = Student_user::orWhere(function ($query)use($search){
                $query->orWhere('student_id','like','%'.$search.'%')
                    ->orWhere('name','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%')
                    ->orWhere('phone','like','%'.$search.'%');
            })->paginate($Number_pen);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤');
        }
        return view('student',['data'=>$data]);
    }

    public function black_list(){
        try{
            $data = Blacklist::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('students_black_list',['data'=>$data]);
    }

    public function black_list_add(Request $request){
        try{
            $check = Student_user::where('student_id',$request->student_id)->firstOrFail();
            $data = new Blacklist;
            $data->student = $check->id;
            $data->save();

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    public function black_list_delete($id){
        try{
            Blacklist::destroy($id);
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    public function drop_mail_config(){
        return view('student_drop_mail_config');
    }

    public function drop_out(){
        return view('student_drop_out');
    }

    public function drop_in(){
        return view('student_drop_in');
    }

    public function drop_all(){
        return view('student_drop_all');
    }
}

<?php

namespace App\Http\Controllers;

use App\AdminUser;
use App\Course_level;
use App\Role_function;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AdminController extends Controller
{
    public function role(){

        return view('admin_role');
    }

    // todo : 寫入資料庫功能
    public function role_add(){
        $function = Role_function::all();
        $class = Course_level::all();
        return view('admin_role_add',['function'=>$function,'class'=>$class]);
    }

    public function role_edit(){
        return view('admin_role_edit');
    }

    public function account(){
        $data = AdminUser::all();
        return view('admin_account');
    }

    public function account_add(Request $request){
        try{
            $data = new AdminUser;
            $data->role = $request->role;
            $data->account = $request->account;
            $data->password = $request->pwd;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }

        return back();
    }

    public function account_edit(){
        return view('admin_account');
    }

    public function account_delete(){
        return view('admin_account');
    }
}

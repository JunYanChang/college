<?php

namespace App\Http\Controllers;

use App\Course_level;
use App\Curriculum;
use App\Student_curricula;
use App\Student_recode;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StudentsUpdateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $level = $request->query('level','');
            $option_level = Course_level::all();
            $curricula = Curriculum::where([['level',$level],['report',1]])->orderBy('compulsory','asc')->get();
        }catch (\Exception $e) {
            Log::info($e->getMessage());
            abort(403,'頁面錯誤！');
        }
        return view('admin.StudentUpdate.students_update',compact('level','option_level','curricula'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Request $request
     * @param $students_update
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$students_update)
    {
        try{
            $selected = $request->selected;
            $status = $request->query('status',1);
            $list = Student_user::where('course_level',$students_update)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            return back();
        }
        return view('students_update_filter',compact('list','selected','students_update','status'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function all_update(Request $request){
        try{
            DB::transaction(function ()use($request){
                foreach ($request->update_list as $l){
                    $data = Student_user::find($l);
                    //紀錄舊級別與學號
                    $old_level = $data->course_level;
                    $old_student_id = $data->student_id;
                    $new_level = Course_level::find($data->course_level + 1);//取得新年級的code

                    $data->student_id = str_replace($data->level->code, $new_level->code, $data->student_id);  //修改學生年級與學號
                    $data->course_level = $data->course_level + 1;
                    $data->save();

                    //新增升級紀錄
                    Student_recode::create(['student'=>$l,'old_level'=>$old_level,'old_student_id'=>$old_student_id]);

                    //todo:寄發升級郵件
                }
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function exception_update(Student_user $id,Request $request){
        try{
            DB::transaction(function ()use($id,$request) {
                //紀錄舊級別與學號
                $old_level = $id->course_level;
                $old_student_id = $id->student_id;
                //判斷升降級
                switch ($request->optradio) {
                    case "update":
                        //取得新年級的code
                        $new_level = Course_level::find($id->course_level+1);
                        //修改學生年級與學號
                        $id->student_id = str_replace($id->level->code,$new_level->code,$id->student_id);
                        $id->course_level = $id->course_level + 1;
                        $id->save();

                        $status_remark = "例外升級";
                        //todo:寄發升級郵件
                        break;
                    case "downgrade":
                        //取得新年級的code
                        $new_level = Course_level::find($id->course_level-1);
                        //修改學生年級與學號
                        $id->student_id = str_replace($id->level->code,$new_level->code,$id->student_id);
                        $id->course_level = $id->course_level - 1;
                        $id->save();

                        //刪除選課
                        Student_curricula::where('student',$id->id)->whereHas('course',function ($query)use($old_level,$new_level){
                            $query->whereBetween('level',[$new_level,$old_level]);
                        })->delete();
                        $status_remark = "例外降級";
                        //todo:寄發升級郵件
                        break;
                }

                $recode = new Student_recode;
                $recode->student = $id->id;
                $recode->old_level = $old_level;
                $recode->old_student_id = $old_student_id;
                $recode->remark = $status_remark;
                $recode->save();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
            return back();
        }
        return back();
    }

    //todo:寄發Email＆升級贈品網址
    public function keyin_exception_update(Request $request){
        try{
            $validatedData = $request->validate([
                'old_id' => 'required|check_student_id',
                'new_id' => 'required|check_level_code'
            ]);

            $level_code = Course_level::where('code',substr($request->new_id,0,2))->firstFail();

            DB::transaction(function ()use($request,$level_code){
                $data = Student_user::where('student_id', $request->old_id)->first();
                $data->student_id = $request->new_id;
                $data->course_level = $level_code->id;
                $data->save();

                $recode = new Student_recode;
                $recode->student = $data->id;
                $recode->old_level = $data->course_level;
                $recode->old_student_id = $request->old_id;
                $recode->remark = "例外升級";
                $recode->save();
                //todo:寄發升級郵件
            });
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }

        return back();
    }
}

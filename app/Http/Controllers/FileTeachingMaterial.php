<?php

namespace App\Http\Controllers;

use App\Course;
use App\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FileTeachingMaterial extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $type = $request->query('type',1);
            $level = Course::all();
            $resources = Resources::where('course',$type)->where('type',2)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.File.file_teaching_material',['level'=>$level,'type'=>$type,'resources'=>$resources]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'course' => 'required',
            'name' => 'required',
            'attr' => 'required',
            'type' => 'required'
        ]);
        try{
            Resources::create($validatedData);
        }catch (\Exception $e){
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Resources $teaching_material
     * @return void
     */
    public function update(Request $request,Resources $teaching_material)
    {
        $validatedData = $request->validate([
            'course' => 'required',
            'name' => 'required',
            'attr' => 'required',
        ]);
        try{
            DB::transaction(function ()use($validatedData,$teaching_material){
                $teaching_material->update($validatedData);
            });
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Resources $teaching_material
     * @return void
     */
    public function destroy(Resources $teaching_material)
    {
        try{
            DB::transaction(function ()use($teaching_material){
                $teaching_material->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Course;
use App\Page;
use App\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FileScriptureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $type = $request->query('type',1);
            $level = Course::all();
            $resources = Resources::where('course',$type)->where('type',1)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.File.file_scripture',compact('type','level','resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'course' => 'required',
            'name' => 'required',
            'attr' => 'required',
            'type' => 'required'
        ]);
        try{
            Resources::create($validatedData);
        }catch (\Exception $e){
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Resources $scripture
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Resources $scripture)
    {
        $validatedData = $request->validate([
            'course' => 'required',
            'name' => 'required',
            'attr' => 'required',
        ]);
        try{
            DB::transaction(function ()use($validatedData,$scripture){
                $scripture->update($validatedData);
            });
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Resources $scripture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Resources $scripture)
    {
        try{
            DB::transaction(function ()use($scripture){
               $scripture->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

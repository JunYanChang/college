<?php

namespace App\Http\Controllers;

use App\AdminUser;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class StuDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::allows('update-post', Student_user::class)) {
            try{
                $request->flash();
                $Number_pen = $request->filled('Number_pen')? $request->Number_pen : 15;
                $search = $request->filled('search') ? $request->search : "";
                $data = Student_user::orWhere(function ($query)use($search){
                    $query->orWhere('student_id','like','%'.$search.'%')
                        ->orWhere('name','like','%'.$search.'%')
                        ->orWhere('email','like','%'.$search.'%')
                        ->orWhere('phone','like','%'.$search.'%');
                })->paginate($Number_pen);
            }catch (\Exception $e){
                Log::info($e->getMessage());
                abort(403,'頁面資訊錯誤');
            }
            return view('admin.StudentData.student',compact('data'));
        }else{
            return "NO";
        }

    }

    /**
     * Display the specified resource.
     *
     * @param Student_user $data
     * @return \Illuminate\Http\Response
     */
    public function show(Student_user $data)
    {
        return view('admin.StudentData.student_detail',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Student_user $data
     * @return \Illuminate\Http\Response
     */
    public function edit(Student_user $data)
    {
        return view('admin.StudentData.student_edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Student_user $data
     * @return Student_user
     */
    public function update(Request $request, Student_user $data)
    {
        try{
            $validatedData = $request->validate([
                'name' => 'required',
                'gender' => 'required',
                'nationality' => 'required',
                'email' => 'required|email',
                'phone' => 'nullable',
                'cellphone' => 'nullable',
                'birthday' => 'date|nullable',
                'address' => 'nullable',
                'language' =>'nullable',
                'fax' => 'nullable',
                'job' => 'nullable',
                'skill' => 'nullable',
                'Volunteer' => 'nullable',
            ]);
            DB::transaction(function ()use($validatedData,$data){
                $data->update($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return redirect()->route('data.show',['data'=>$data]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @param Student_user $data
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student_user $data)
    {
        try{
            DB::transaction(function ()use($data){
                $data->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

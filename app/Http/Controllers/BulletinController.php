<?php

namespace App\Http\Controllers;

use App\Bulletin;
use App\Bulletin_replies;
use App\Bulletin_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class BulletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $select = $request->query('id',1);
            $type = Bulletin_type::all();
            $news = Bulletin::where('type',$select)->get();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            abort(403, '頁面資訊錯誤');
        }
        return view('admin.Bulletin.announcement', compact('type','select','news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            DB::transaction(function ()use($request){
                $validatedData = $request->validate([
                    'type' => 'required',
                    'title' => 'required',
                    'content' => 'required',
                ]);
                Bulletin::create($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Bulletin $bulletin
     * @return \Illuminate\Http\Response
     */
    public function show(Bulletin $bulletin)
    {
        return view('admin.Bulletin.replies',compact('bulletin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Bulletin $bulletin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Bulletin $bulletin)
    {
        try{
            DB::transaction(function ()use($request,$bulletin){
                $validatedData = $request->validate([
                    'type' => 'required',
                    'title' => 'required',
                    'content' => 'required',
                ]);
                $bulletin->update($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Bulletin $bulletin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bulletin $bulletin)
    {
        try{
            DB::transaction(function ()use($bulletin){
               $bulletin->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    public function create_type(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
        ]);
        Bulletin_type::create($validatedData);

        return back();
    }

    public function delete_relies(Bulletin_replies $replies){
        $replies->delete();
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = Course::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.Course.course',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.Course.course_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validatedData = $request->validate([
                'name' => 'required|check_course_name',
                'intro' => 'required'
            ]);
            DB::transaction(function ()use($validatedData){
                Course::create($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return redirect()->route('course.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Course $course
     * @return void
     */
    public function edit(Course $course)
    {
        return view('admin.Course.course_edit',compact('course'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param Course $course
     * @return void
     */
    public function update(Request $request, Course $course)
    {
         try{
             $validatedData = $request->validate([
                 'name' => 'required',
                 'intro' => 'required'
             ]);
             DB::transaction(function ()use($course,$validatedData){
                $course->update($validatedData);
             });
             DB::commit();
         }catch (\Exception $e){
             Log::info($e->getMessage());
             DB::rollBack();
         }

         return redirect()->route('course.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course $course
     * @return void
     * @throws \Exception
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return back();
    }
}

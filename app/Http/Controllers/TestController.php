<?php

namespace App\Http\Controllers;

use App\Classes\txtRead;
use App\Imports\UsersImport;
use App\Report;
use App\Student_user;
use App\Classes\utf8_chinese;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;


class TestController extends Controller
{
    public function test(){
//        $encrypted = Crypt::encryptString('Hello world.');
//        $decrypted = Crypt::decryptString($encrypted);
        $hashed = Hash::make('password', [
            'rounds' => 12
        ]);
        if(Hash::check('password',$hashed)){
            return "is same";
        }else{
            return "no";
        }
//        return response('Hello World', 200)->header('Content-Type', 'text/plain');
    }

    public function testshow(){
        Log::info("Showing user profile for user");
        return "123";
    }

}

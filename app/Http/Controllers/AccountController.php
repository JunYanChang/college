<?php

namespace App\Http\Controllers;

use App\AdminRole;
use App\AdminUser;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = AdminUser::all();
        $role = AdminRole::all();
        return view('admin.Adminstration.admin_account',compact('data','role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'account' => 'required|check_account',
            'password' => 'required',
            'role' => 'required'
        ]);
        try{
            DB::transaction(function ()use($request){
                $data = new AdminUser;
                $data->name = $request->name;
                $data->account = $request->account;
                $data->password = encrypt($request->password);
                $data->role = $request->role;
                $data->save();
            });
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }

        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param AdminUser $account
     * @return void
     */
    public function update(Request $request, AdminUser $account)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'account' => 'required',
            'role' => 'required'
        ]);

        try{
            DB::transaction(function ()use($account,$request){
               $account->name = $request->name;
               $account->account = $request->account;
               if($request->has('passsword')){
                   $account->password = encrypt($request->password);
               }
               $account->role = $request->role;
               $account->save();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AdminUser $account
     * @return void
     */
    public function destroy(AdminUser $account)
    {
        try{
            DB::transaction(function ()use($account){
                $account->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

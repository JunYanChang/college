<?php

namespace App\Http\Controllers;

use App\Report;
use App\Student_curricula;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class StudentGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $request->flash();
            $search = $request->query('search',"");
            $data = Student_user::where('student_id',$search)->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤！');
        }
        return view('admin.StudentGrade.students_grade',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Report $students_grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $students_grade)
    {
        return View('admin.StudentGrade.students_grade_respond',compact('students_grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Report $students_grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $students_grade)
    {
        try{
            DB::transaction(function ()use($request,$students_grade){
                $students_grade->update(['grade'=>$request->score,'respond'=>$request->respond]);

                $student_curriculas = Student_curricula::where([['student',$students_grade->student],['curricula',$students_grade->curricula]])->first();
                $student_curriculas->grade = $request->score;
                if($request->score == "D" || $request->score == "E" || $request->score == "F"){
                    //todo::增加退回報告時增加email回信，使用回應的內容為信件內文
                    $student_curriculas->done = 0;
                }else{
                    $student_curriculas->done = 1;
                }
                $student_curriculas->save();
            });
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

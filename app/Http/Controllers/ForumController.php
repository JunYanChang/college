<?php

namespace App\Http\Controllers;

use App\Blacklist;
use App\Page;
use App\Student_user;
use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ForumController extends Controller
{
    public function theme(){
        $data = Theme::all();
        return view('forum_theme',['data'=>$data]);
    }

    public function theme_add(Request $request){
        try{
            $data = new Theme;
            $data->name = $request->name;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }

        return back();
    }

    public function theme_edit($id,Request $request){
        try{
            $data = Theme::find($id);
            $data->name = $request->name;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function theme_delete($id){
        try{
            Theme::destroy($id);
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\AdminUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    protected $guard = 'web';

    protected $redirecTo = '/dashboard';
    public function username()
    {
        return 'account';
    }

    public function login(){
        return view('admin.login');
    }

    public function validatedData(Request $request){
        $validatedData = $request->validate([
            'account' => 'required',
            'password' => 'required',
        ]);
        if(auth()->guard('web')->attempt(['account'=>$request->account,'password'=>$request->password])){
            return "OK";
        }
//        $check_password = AdminUser::where('account',$request->account)->first();
//        if(decrypt($check_password->password) != $request->password){
//            return back();
//        }
//        if($request->remember == 'on'){
//            Cookie::queue('token', $request->_token);
//            $check_password->update(['remember_token'=>$request->_token]);
//        }
//
//        //session
//        Session::put('user',$check_password);
//
//        return redirect()->route('dashboard.index');
    }

    public function logout(AdminUser $user){
        $user->update(['remember_token'=>'']);
        Cookie::queue(Cookie::forget('token'));
        Session::forget('user');
        return redirect()->route('login');
    }
}

<?php

namespace App\Http\Controllers;

use App\Course;
use App\Course_level;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Classes\toChineseNumber;

class CourseLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = Course::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.Course_level.course_level',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try{
            $num2Zh = new toChineseNumber();
            switch (Course::find($request->course)->name){
                case "基礎學科":
                    $level = "基礎班".$num2Zh->num2zh(Course_level::where('courses',$request->course)->count() + 1)."年級";
                    break;
                case "本科學科":
                    $level = "本科班".$num2Zh->num2zh(Course_level::where('courses',$request->course)->count() + 1)."年級";
                    break;
                case "專科學科":
                    $level = "專科班".$num2Zh->num2zh(Course_level::where('courses',$request->course)->count() + 1)."年級";
                    break;
            }
            $Course_level = new Course_level();
            $Course_level->courses = $request->course;
            $Course_level->level = $level;
            $Course_level->save();
            DB::commit();

        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function destroy(Course_level $course_level)
    {
        $course_level->delete();
        return back();
    }
}

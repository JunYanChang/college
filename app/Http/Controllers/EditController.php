<?php

namespace App\Http\Controllers;

use App\Calendar;
use App\Config;
use App\Page;
use App\Theme;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EditController extends Controller
{
    //入學指導
    public function admission_guidance(){
        try{
            $data = Page::where('item','guidance')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_admission_guidance',compact('data'));
    }

    //教學計畫
    public function plan(){
        try{
            $data = Page::where('item','plan')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_plan',compact('data'));
    }

    //認識學院
    public function understanding(Request $request){
        try{
            $type = $request->query('type','understanding_origin');
            $data = Page::where('item',$type)->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_understanding',compact('data','type'));
    }

    //說明看板
    public function description(){
        try{
            $classification = Page::where('item','classification')->first();
            $precautions = Page::where('item','precautions')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_description',compact('classification','precautions'));
    }

    //訊息公告
    public function announcement(){
        try{
            $config = Config::where('title','announcement')->first();
            $data = Page::where('item','announcement')->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_announcement',compact('config','data'));
    }

    public function edit(Request $request,$type){
        try{
            $check = Page::where('item',$type)->first();
            if(empty($check)){
                $data = new Page();
                $data->item = $type;
                $data->content = $request->input('content');
                switch ($type){
                    case "understanding_origin":
                        $data->remark = '認識學院_成立緣起';
                        break;
                    case "understanding_purpose":
                        $data->remark = '認識學院_教學宗旨';
                        break;
                    case "understanding_teach":
                        $data->remark = '認識學院_學院院訓';
                        break;
                    case "understanding_way":
                        $data->remark = '認識學院_教學方式';
                        break;
                    case "understanding_introduction":
                        $data->remark = '認識學院_修學介紹';
                        break;
                    case "understanding_future":
                        $data->remark = '認識學院_未來展望';
                        break;
                    case "guidance":
                        $data->remark = '入學指導';
                        break;
                    case "plan":
                        $data->remark = '教學計劃';
                        break;
                    case "classification":
                        $data->remark = "說明看板-分類說明";
                        break;
                    case "precautions":
                        $data->remark = "說明看板-注意事項";
                        break;
                    case "announcement":
                        $data->remark = '重要通知';
                        break;
                }
                $data->save();
            }else{
                $validatedData = $request->validate([
                    'content' => 'required',
                ]);
                $check->update($validatedData);
            }
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }


    public function announcement_mange(Request $request,Config $config){
        try{
            $validatedData = $request->validate([
                'config' => 'required',
            ]);
            DB::transaction(function ()use($config,$validatedData){
                $config->update($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

}

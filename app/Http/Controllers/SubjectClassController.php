<?php

namespace App\Http\Controllers;

use App\course_data;
use App\Course_level;
use App\Curriculum;
use App\Rules\Check_Course;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SubjectClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $id = $request->query('class',1);
            $level = Course_level::all();
            $data = Course_level::find($id);
            $course = course_data::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.Subject_class.subject_class',compact('level','data','course','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $validatedData = $request->validate([
                'level' => 'required',
                'course_data' => ['required',new Check_Course($request->level)],
                'report' => 'required',
                'compulsory' => 'required',
                'remark' => 'nullable'
            ]);

            DB::transaction(function ()use($validatedData){
                Curriculum::create($validatedData);
            });

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Curriculum $subject_class
     * @return void
     */
    public function edit(Curriculum $subject_class)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Curriculum $subject_class
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Curriculum $subject_class)
    {
        try{
            $validatedData = $request->validate([
                'level' => 'required',
                'course_data' => 'required',
                'report' => 'required',
                'compulsory' => 'required',
                'remark' => 'nullable'
            ]);

            DB::transaction(function ()use($validatedData,$subject_class){
                $subject_class->update($validatedData);
            });

            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Curriculum $subject_class
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Curriculum $subject_class)
    {
        try{
            DB::transaction(function ()use($subject_class){
                $subject_class->delete();
            });
            DB::commit();
        }catch (\Exception $e){
            DB::rollBack();
            Log::info($e->getMessage());
        }
        return back();
    }
}

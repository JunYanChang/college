<?php

namespace App\Http\Controllers;

use App\Course;
use App\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FileController extends Controller
{
    //經本區
    public function scripture($type = 1){
        try{
            $level = Course::all();
            $resources = Resources::where('course',$type)->where('type',1)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('file_scripture',['level'=>$level,'type'=>$type,'resources'=>$resources]);
    }

    public function scripture_add(Request $request){
        try{
            $data = new Resources;
            $data->course = $request->class;
            $data->type = 1;
            $data->name = $request->title;
            $data->attr = $request->url;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function scripture_edit($type = 1,$id,Request $request){
        try{
            $data = Resources::find($id);
            $data->course = $request->class;
            $data->name = $request->title;
            $data->attr = $request->url;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function scripture_delete($type = 1,$id){
        try{
            Resources::destroy($id);
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    //教材區
    public function teaching_material($type = 1){
        try{
            $level = Course::all();
            $resources = Resources::where('course',$type)->where('type',2)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('file_teaching_material',['level'=>$level,'type'=>$type,'resources'=>$resources]);
    }

    public function teaching_material_add(Request $request){
        try{
            $data = new Resources;
            $data->course = $request->class;
            $data->type = 2;
            $data->name = $request->title;
            $data->attr = $request->url;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function teaching_material_edit($type = 1,$id,Request $request){
        try{
            $data = Resources::find($id);
            $data->course = $request->class;
            $data->name = $request->title;
            $data->attr = $request->url;
            $data->save();
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    public function teaching_material_delete($type = 1,$id){
        try{
            Resources::destroy($id);
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }
}

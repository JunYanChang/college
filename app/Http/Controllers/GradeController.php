<?php

namespace App\Http\Controllers;

use App\Classes\Url;
use App\Course;
use App\Course_level;
use App\Curriculum;
use App\Report;
use App\Student_curricula;
use App\Student_user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class GradeController extends Controller
{
    //todo:增加退回報告時增加email回信，使用回應的內容為信件內文
    public function work_grade($status = '',Request $request){
        try{
            $s_level = $request->has('level') ? $request->level: 1;
            $s_curricula = $request->has('curriculum') ? $request->curriculum : 2;
            $level = Course_level::all();
            $curricula = Curriculum::where([['level',$s_level],['report',1]])->get();
            $report = Report::where('curricula',$s_curricula)->where('status','like',$status.'%')->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤！');
        }
        return view('work_grade',[
            'level'=>$level,
            'curricula'=>$curricula,
            's_level'=>$s_level,
            's_curricula'=>$s_curricula,
            'report'=>$report,
            'status'=>$status
        ]);
    }

    public function work_grade_respond($id){
        $report = Report::find($id);
        return view('work_grade_respond',['report'=>$report]);
    }

    public function work_grade_score($type,$id,Request $request){
        try{
            $data = Report::find($id);
            $student_curriculas = Student_curricula::where([['student',$data->student],['curricula',$data->curricula]])->first();
            switch ($type){
                case "fast":
                    DB::transaction(function ()use($data,$student_curriculas,$request){
                        $data->status = 1;
                        $data->grade = $request->score;
                        $data->save();

                        $student_curriculas->grade = $request->score;
                        if($request->score == "D" || $request->score == "E" || $request->score == "F"){
                            $student_curriculas->done = 0;
                        }else{
                            $student_curriculas->done = 1;
                        }
                        $student_curriculas->save();
                    });
                    break;
                case "general":
                    DB::transaction(function ()use($data,$student_curriculas,$request){
                        $data->status = 1;
                        $data->grade = $request->score;
                        $data->respond = $request->respond;
                        $data->save();

                        $student_curriculas->grade = $request->score;
                        if($request->score == "D" || $request->score == "E" || $request->score == "F"){
                            $student_curriculas->done = 0;
                        }else{
                            $student_curriculas->done = 1;
                        }
                        $student_curriculas->save();
                    });
                    break;
            }
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return redirect('/admin/Grade/work_grade/list?level='.$data->course->level.'&curriculum='.$data->curricula);

    }

    public function work_grade_unreport(Request $request){
        try{
            $level = Course_level::all();
            $curricula = Curriculum::where([['level',$request->level],['report',1]])->get();
            $data = Curriculum::find($request->curricula);
        }catch (\Exception $e){
            Log::info($e->getMessage());
            return back();
        }

        return view('work_grade_unreport',[
            'level' => $level,
            'curricula' => $curricula,
            'data' => $data ,
        ]);
    }

    public function students_grade(Request $request){
        try{
            $search = $request->search ??  "";
            $data = Student_user::where('student_id',$search)->first();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤！');
        }
        return view('students_grade',['data' => $data]);
    }

    public function students_grade_score($id,Request $request){
        try{
            DB::transaction(function ()use($id,$request){
                $data = Student_curricula::find($id);
                $data->done = 1;
                $data->grade = $request->score;
                $data->save();

                $report = Report::where([['student',$data->student],['curricula',$data->curricula]])->first();
                $report->grade = $request->score;
                if($request->score == "D" || $request->score == "E" || $request->score == "F"){
                    $report->status = 0;
                }else{
                    $report->status = 1;
                }
                $report->save();
            });

            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();

    }

    public function de_sing(){
        return view('de_sing');
    }

    public function de_sing_list_grade(){
        return view('de_sing_list_grade');
    }
}

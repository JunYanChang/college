<?php

namespace App\Http\Controllers;

use App\Course_data;
use App\Host;
use App\Page;
use App\Classes\txtRead;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CourseDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $data = Course_data::all();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.Course_data.course_data',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try{
            $validatedData = $request->validate([
                'sn' => 'required',
                'title' => 'required',
                'teacher' => 'required',
                'separation' => 'nullable',
                'start_ep' => 'required',
                'end_ep' => 'required',
                'type' => 'required',
                'introduction' => 'required'
            ]);
            DB::transaction(function ()use($validatedData){
                course_data::create($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param Course_data $course_datum
     * @return course_data
     */
    public function show(Course_data $course_datum,Request $request)
    {

        try{
            $data = $course_datum->get_media()->paginate(6);
            $host = Host::find(1)->attr;
            if($request->has('host')){
                $host = Host::find($request->host)->attr;
            }
            $txt = new txtRead();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.Course_data.course_media',compact('data','txt','host','course_datum'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Course_data $course_datum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, course_data $course_datum)
    {
        try{
            $validatedData = $request->validate([
                'sn' => 'required',
                'title' => 'required',
                'teacher' => 'required',
                'separation' => 'nullable',
                'start_ep' => 'required',
                'end_ep' => 'required',
                'type' => 'required',
                'introduction' => 'nullable'
            ]);
            DB::transaction(function ()use($validatedData,$course_datum){
                $course_datum->update($validatedData);
            });
            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Course_data $course_datum
     * @return void
     */
    public function destroy(course_data $course_datum)
    {
        $course_datum->delete();
        return back();
    }
}

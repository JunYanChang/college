<?php

namespace App\Http\Controllers;

use App\Calendar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class EditCalendarController extends Controller
{
    public function calendar(Request $request){
        try{
            $year = $request->query('year',date('Y'));
            $data = Calendar::where('year',$year)->get();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面資訊錯誤！');
        }
        return view('admin.EditPage.edit_calendar',compact('data'));
    }

    public function calendar_save(Request $request,$year,$month){
        try{
            $check = Calendar::where('year',$year)->where('month',$month)->first();
            if(empty($check)) {
                $data = new Calendar;
                $data->year = $year;
                $data->month = $month;
                $data->list = $request->list;
                $data->save();
            }else{
                $validatedData = $request->validate([
                    'list' => 'required',
                ]);
                $check->update($validatedData);
            }

            DB::commit();
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }

        return back();
    }
}

<?php

namespace App\Http\Controllers;

use App\Course_level;
use App\Curriculum;
use App\Report;
use App\Student_curricula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class WorkGradeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try{
            $status = $request->query('status','');
            $s_level = $request->query('level',1);
            $s_curricula = $request->query('curriculum',2);

            $level = Course_level::all();
            $curricula = Curriculum::where([['level',$s_level],['report',1]])->get();
            $report = Report::where('curricula',$s_curricula)->where('status','like',$status.'%')->get();

        }catch (\Exception $e){
            Log::info($e->getMessage());
            abort(403,'頁面錯誤.');
        }
        return view('admin.WorkGrade.work_grade',compact('status','curricula','s_level','s_curricula','level','report'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Report $work_grade
     * @return \Illuminate\Http\Response
     */
    public function show(Report $work_grade)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Report $work_grade
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $work_grade)
    {
        return view('admin.WorkGrade.work_grade_respond',compact('work_grade'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Report $work_grade
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $work_grade)
    {
        try{
            DB::transaction(function ()use($request,$work_grade){
                $work_grade->update(['grade'=>$request->score,'respond'=>$request->respond]);

                $student_curriculas = Student_curricula::where([['student',$work_grade->student],['curricula',$work_grade->curricula]])->first();
                $student_curriculas->grade = $request->score;
                if($request->score == "D" || $request->score == "E" || $request->score == "F"){
                    //todo::增加退回報告時增加email回信，使用回應的內容為信件內文
                    $student_curriculas->done = 0;
                }else{
                    $student_curriculas->done = 1;
                }
                $student_curriculas->save();
            });
        }catch (\Exception $e){
            Log::info($e->getMessage());
            DB::rollBack();
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

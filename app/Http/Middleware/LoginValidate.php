<?php

namespace App\Http\Middleware;

use App\AdminUser;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Closure;

class LoginValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Cookie::has('token')){
            if(AdminUser::where('remember_token',Cookie::get('token'))->first() == true ){
                return redirect()->route('dashboard.index');
            }
        }
        if(Session::has('user')){
            return redirect()->route('dashboard.index');
        }

        return $next($request);
    }
}

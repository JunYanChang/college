<?php

namespace App\Http\Middleware;

use App\AdminUser;
use Closure;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;

class ValidateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Cookie::has('token')){
            if(AdminUser::where('remember_token',Cookie::get('token'))->first() == false){
                return redirect()->route('login');
            }else{
                if(!Session::has('user')){
                    Session::put('user',AdminUser::where('remember_token',Cookie::get('token'))->first());
                }
            }
            return $next($request);
        }else if(Session::has('user')){
            return $next($request);
        }else{
            return redirect()->route('login');
        }

    }
}

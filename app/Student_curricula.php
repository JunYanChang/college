<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student_curricula extends Model
{
    protected $guarded = [];

    public function course(){
        return $this->belongsTo('App\Curriculum','curricula');
    }

    public function reports(){
        return $this->hasMany('App\Report','student','student');
    }

    public function get_student(){
        return $this->belongsTo('App\Student_user','student');
    }

}
